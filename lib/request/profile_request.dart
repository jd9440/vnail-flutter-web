// Generated by https://quicktype.io
// To parse this JSON data, do
//
//     final logInRequest = profileRequestFromJson(jsonString);

import 'dart:convert';

ProfileRequest profileRequestFromJson(String str) {
  final jsonData = json.decode(str);
  return ProfileRequest.fromJson(jsonData);
}

String profileRequestToJson(ProfileRequest data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class ProfileRequest {
  String? jsonrpc;
  Params? params;

  ProfileRequest({
    this.jsonrpc,
    this.params,
  });

  factory ProfileRequest.fromJson(Map<String, dynamic> json) =>
      new ProfileRequest(
        jsonrpc: json["jsonrpc"],
        params: Params.fromJson(json["params"]),
      );

  Map<String, dynamic> toJson() => {
        "jsonrpc": jsonrpc,
        "params": params!.toJson(),
      };
}

class Params {
  String db;
  int? uid;

  Params({
    this.db = "vnail_2",
    required this.uid,
  });

  factory Params.fromJson(Map<String, dynamic> json) => new Params(
        db: json["db"],
        uid: json["uid"],
      );

  Map<String, dynamic> toJson() => {
        "db": db,
        "uid": uid,
      };
}
