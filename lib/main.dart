import 'dart:ui'; 

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/generated/codegen_loader.g.dart';
import 'package:vnail/pages/home_page.dart';
import 'package:vnail/providers/cart_provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/providers/product_provider.dart';
import 'package:vnail/providers/wishlist_provider.dart';
import 'package:vnail/utility/theme.dart';

import 'utility/DBUtils.dart';
import 'utility/path.dart';

void main() {
  initialization();
}

Future<void> initialization() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  await Hive.initFlutter();
  await Hive.openBox<String>(DBUtils.userBox);

  runApp(
    EasyLocalization(
      path: Paths.translations,
      supportedLocales: [
        Locale('zh'),
        Locale('en'),
      ],
      startLocale: Locale('zh'),
      useOnlyLangCode: true,
      child: MyApp(),
      assetLoader: CodegenLoader(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
        BouncingScrollWrapper.builder(context, widget!),
        maxWidth: 2460,
        minWidth: 200,
        alignment: Alignment.center,
        breakpoints: [
          ResponsiveBreakpoint.autoScaleDown(
            200,
            name: MOBILE,
            scaleFactor: 0.2,
          ),
          ResponsiveBreakpoint.autoScale(
            450,
            name: MOBILE,
            scaleFactor: 0.4,
          ),
          ResponsiveBreakpoint.autoScale(
            800,
            name: TABLET,
            scaleFactor: 0.75,
          ),
          ResponsiveBreakpoint.autoScale(
            1200,
            name: DESKTOP,
            scaleFactor: 1,
          ),
          ResponsiveBreakpoint.autoScale(
            2460,
            name: DESKTOP,
            scaleFactor: 1.15,
          ),
        ],
        background: Container(
          color: Color(0xFFF5F5F5),
        ),
      ),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'VNail',
      theme: vnailTheme,
      debugShowCheckedModeBanner: false,
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => HomePageProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => WishListProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => CartProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => ProductProvider(context),
          ),
        ],
        child: HomePage(),
      ),
    );
  }
}
