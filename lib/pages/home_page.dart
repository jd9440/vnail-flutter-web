import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/widgets/footer.dart';
import 'package:vnail/widgets/header.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<HomePageProvider>();
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: [
          HeaderWidget(),
          _provider.homePageContent,
          FooterWidget(),
        ],
      ),
    );
  }
}
