class ProductList {
  ProductList({
    required this.jsonrpc,
    this.id,
    required this.result,
  });
  late final String jsonrpc;
  late final Null id;
  late final List<Result> result;

  ProductList.fromJson(Map<String, dynamic> json) {
    jsonrpc = json['jsonrpc'];
    id = null;
    result = List.from(json['result']).map((e) => Result.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['jsonrpc'] = jsonrpc;
    _data['id'] = id;
    _data['result'] = result.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Result {
  Result({
    required this.id,
    required this.name,
    required this.description,
    required this.currency,
    required this.productCategary,
    required this.price,
    required this.subTotal,
  });
  late final int id;
  late final String name;
  late final String description;
  late final String currency;
  late final String productCategary;
  late final int price;
  late int quantity, subTotal;

  Result.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = (json['description'] == false) ? "" : json['description'];
    currency = json['currency'];
    productCategary = json['product_categary'];
    price = json['price'];
    quantity = json['quantity'] ?? 1;
    subTotal = json['subTotal'] ?? price * quantity;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['description'] = description;
    _data['currency'] = currency;
    _data['product_categary'] = productCategary;
    _data['price'] = price;
    _data['quantity'] = 1;
    _data['subTotal'] = price * quantity;
    return _data;
  }
}
