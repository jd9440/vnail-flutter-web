import 'package:cool_alert/cool_alert.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/network/api_provider.dart';
import 'package:vnail/utility/DBUtils.dart';
import 'package:vnail/widgets/alertdialogbody.dart';
import 'package:vnail/widgets/homecontent.dart';

class HomePageProvider extends ChangeNotifier {
  late bool obscureText, signUpCheckBoxValue;

  int currentAlertDialogWidgetIndex = 0,
      currentIndexProduct = 0,
      productCountDescription = 0,
      currentProfileWidget = 0;

  final APIProvider _apiProvider = APIProvider();

  late Image currentSelectedBrandImage;

  Widget _homePageContent = HomeContent();

  Widget get homePageContent => _homePageContent;

  void updateHomeContent(Widget updatedWidget) {
    _homePageContent = updatedWidget;
    notifyListeners();
  }

  HomePageProvider() {
    obscureText = true;
    signUpCheckBoxValue = false;
    currentSelectedBrandImage = Image.asset(Assets.images.adorelle.path);
  }

  toggleObscureValue() {
    obscureText = !obscureText;
    notifyListeners();
  }

  toggleSignUpValue() {
    signUpCheckBoxValue = !signUpCheckBoxValue;
    notifyListeners();
  }

  changeIndex(int indexNumber) {
    currentAlertDialogWidgetIndex = indexNumber;
    notifyListeners();
  }

  Future<void> showLogInAlertDialog(
    BuildContext context,
  ) async {
    return await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          content: ChangeNotifierProvider(
            create: (context) => HomePageProvider(),
            child: AlertDialogBodyWidget(),
          ),
        );
      },
    ).then((value) {});
  }

  void popDialog(BuildContext context) {
    currentAlertDialogWidgetIndex = 0;
    Navigator.pop(context);
  }

  void updateBrandContent(int index) {
    switch (index) {
      case 0:
        currentSelectedBrandImage = Image.asset(Assets.images.adorelle.path);
        break;
      case 1:
        currentSelectedBrandImage = Image.asset(Assets.images.bd.path);
        break;
      case 2:
        currentSelectedBrandImage = Image.asset(Assets.images.cuccio.path);
        break;
      case 3:
        currentSelectedBrandImage = Image.asset(Assets.images.footlogix.path);
        break;
      default:
        currentSelectedBrandImage = Image.asset(Assets.images.mainbeach.path);
    }
    notifyListeners();
  }

  Image get currentBrandImage => currentSelectedBrandImage;

  void increaseProductCountDescription() {
    productCountDescription++;
    notifyListeners();
  }

  void decreaseProductCountDescription() {
    if (productCountDescription > -1) {
      productCountDescription--;
      notifyListeners();
    }
  }

  void changeCurrentProfileWidget(int index) {
    currentProfileWidget = index;
    notifyListeners();
  }

  int get currentProfileWidgetIndex => currentProfileWidget;

  Future<void> logAndGetUserData(
    BuildContext context,
    String emailAddress,
    String password,
  ) async {
    _apiProvider.logInUser(context, emailAddress, password);
  }

  Future<void> clearDataAndLogOut(BuildContext context) async {
    CoolAlert.show(
      context: context,
      type: CoolAlertType.warning,
      text: tr(LocaleKeys.logout_msg),
      onConfirmBtnTap: () {
        DBUtils().clearAllDataAndLogOut();
        Navigator.pop(context);
        _homePageContent = HomeContent();
        notifyListeners();
      },
      showCancelBtn: true,
      onCancelBtnTap: () => Navigator.pop(context),
    );
  }
}
