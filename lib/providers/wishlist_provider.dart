import 'package:flutter/material.dart';
import 'package:vnail/response/product_response.dart';

class WishListProvider extends ChangeNotifier {
  List<Result> _wishList = [];

  bool checkIfAlreadyExists(Result productTobeInserted){
    for(Result product in _wishList){
      if(productTobeInserted.name==product.name){
        return true;
      }
    }
    return false;
  }

  void addProductToWishList(Result individualProduct) {
    _wishList.add(individualProduct);
    notifyListeners();
  }

  void removeProductFromWishList(Result individualProduct) {
    _wishList.remove(individualProduct);
    notifyListeners();
  }

  List<Result> get wishListItems => _wishList;
}
