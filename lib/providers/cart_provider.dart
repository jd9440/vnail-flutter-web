import 'package:flutter/material.dart';
import 'package:vnail/network/api_provider.dart';
import 'package:vnail/podo/userdata.dart';
import 'package:vnail/request/checkout_request.dart';
import 'package:vnail/response/product_response.dart';
import 'package:vnail/utility/DBUtils.dart';

class CartProvider extends ChangeNotifier {
  List<Result> _cartItems = [];
  List<Result> get cartItems => _cartItems;

  APIProvider _apiProvider = APIProvider();

  bool _sameShippingAddress = false;
  bool _isUsingLoyaltyPoints = false;
  int _totalItems = 0;

  String billingName = "";
  String billingPhoneNumber = "";
  String billingEmail = "";
  String billingStreetNumber = "";
  String billingStreet = "";
  String billingCity = "";

  String shippingName = "";
  String shippingPhoneNumber = "";
  String shippingEmail = "";
  String shippingStreetNumber = "";
  String shippingStreet = "";
  String shippingCity = "";

  int shippingCharge = 0;
  double loyaltyPointConversion = 0, _totalPrice = 0;

  bool checkIfAlreadyExists(Result productTobeInserted) {
    for (Result product in _cartItems) {
      if (productTobeInserted.name == product.name) {
        return true;
      }
    }
    return false;
  }

  void addProductToCart(Result individualProduct) {
    _cartItems.add(individualProduct);
    _totalItems++;
    _totalPrice += (individualProduct.quantity * individualProduct.price);
    notifyListeners();
  }

  void removeProductFromCart(Result individualProduct) {
    _totalItems -= individualProduct.quantity;
    _totalPrice -= (individualProduct.quantity * individualProduct.price);

    //  resetting the item quantity to 1.
    individualProduct.quantity = 1;
    _cartItems.remove(individualProduct);
    notifyListeners();
  }

  increaseVolume(int index) {
    Result _individualItem = _cartItems[index];
    _individualItem.quantity++;
    _totalItems++;
    _totalPrice += _individualItem.price;
    notifyListeners();
  }

  decreaseVolume(int index) {
    Result _individualItem = _cartItems[index];
    int individualQuantity = _individualItem.quantity;
    if (individualQuantity > 1) {
      _individualItem.quantity--;
      _totalItems--;
      _totalPrice -= _individualItem.price;
      notifyListeners();
    }
  }

  bool ifCartEmpty() {
    return _cartItems.isEmpty;
  }

  void toggleUsingLoyaltyPoints() {
    UserData _data = getUserData();

    loyaltyPointConversion = _data.loyaltyPoints * 0.2;
    if (_isUsingLoyaltyPoints) {
      _totalPrice += loyaltyPointConversion;
    } else {
      _totalPrice -= loyaltyPointConversion;
    }

    _isUsingLoyaltyPoints = !_isUsingLoyaltyPoints;
    notifyListeners();
  }

  bool get isUsingLoyaltyPoints => _isUsingLoyaltyPoints;

  void addWishlistToCartItems(List<Result> wishList) {
    _cartItems.clear();
    _cartItems.addAll(wishList);
    _totalPrice = 0;
    _totalItems = wishList.length;
    for (var item in wishList) {
      _totalPrice += (item.price * item.quantity);
    }
    notifyListeners();
  }

  setCartItems(Result item) {
    _cartItems.clear();
    _cartItems.add(item);
    _totalItems = 1;
    _totalPrice = (item.price * item.quantity).toDouble();
    notifyListeners();
  }

  void toggleShippingAddress(bool? value) {
    _sameShippingAddress = value ?? false;
    notifyListeners();
  }

  int get totalItem => _totalItems;
  double get totalPrice => _totalPrice;
  bool get sameShippingAddress => _sameShippingAddress;

  void shippingAddressFalse() {
    _sameShippingAddress = false;
  }

  int getShippingCharge() {
    if (totalPrice < 800) {
      return 30;
    }
    return 0;
  }

  void setBillingData(
    String billingName,
    String billingPhoneNumber,
    String billingEmail,
    String billingStreetNumber,
    String billingStreet,
    String billingCity,
    String shippingName,
    String shippingPhoneNumber,
    String shippingEmail,
    String shippingStreetNumber,
    String shippingStreet,
    String shippingCity,
  ) {
    this.billingName = billingName;
    this.billingPhoneNumber = billingPhoneNumber;
    this.billingEmail = billingEmail;
    this.billingStreetNumber = billingStreetNumber;
    this.billingStreet = billingStreet;
    this.billingCity = billingCity;

    this.shippingName = shippingName;
    this.shippingPhoneNumber = shippingPhoneNumber;
    this.shippingEmail = shippingEmail;
    this.shippingStreetNumber = shippingStreetNumber;
    this.shippingStreet = shippingStreet;
    this.shippingCity = shippingCity;
  }

  Future<String> openCheckoutPage(BuildContext context) async {
    List<CheckoutRequest> _request = [];
    for (var item in _cartItems) {
      _request.add(
        CheckoutRequest(
          priceData: PriceData(
            currency: 'hkd',
            productData: ProductData(
              name: item.name,
            ),
            unitAmount: item.price * 100,
          ),
          quantity: item.quantity,
        ),
      );
    }
    if (getShippingCharge() > 0) {
      _request.add(
        CheckoutRequest(
          priceData: PriceData(
              currency: 'hkd',
              productData: ProductData(
                name: 'Shipping Charge',
              ),
              unitAmount: 3000),
          quantity: 1,
        ),
      );
    }
    return _apiProvider.checkOutAPI(context, _request);
  }

  UserData getUserData() => DBUtils().getUserData();
}
