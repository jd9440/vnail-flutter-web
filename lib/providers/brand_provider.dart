import 'package:flutter/material.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/utility/device_utils.dart';

class BrandProvider extends ChangeNotifier {
  int _selectionBrandIndex = 0;

  void changeBrandSelection(int index) {
    _selectionBrandIndex = index;
    notifyListeners();
  }

  Widget _emptyWidget = SizedBox();

  get currentBrandIndex => _selectionBrandIndex;
  get emptyWidget => _emptyWidget;

  String getImagePath(BuildContext context) {
    switch (_selectionBrandIndex) {
      case 0:
        _emptyWidget = SizedBox(
          height: DeviceUtils.isMobileDevice(context) >= 3000 ? 1800 : 
          DeviceUtils.isMobileDevice(context) >= 2500 && DeviceUtils.isMobileDevice(context) < 3000 ? 1200 : 
          DeviceUtils.isMobileDevice(context) >= 2300 && DeviceUtils.isMobileDevice(context) < 2500 ? 900 : 
          DeviceUtils.isMobileDevice(context) >= 2000 && DeviceUtils.isMobileDevice(context) < 2300 ? 630 : 
          DeviceUtils.isMobileDevice(context) >= 1700 && DeviceUtils.isMobileDevice(context) < 2000 ? 330 :
          DeviceUtils.isMobileDevice(context) >= 1500 && DeviceUtils.isMobileDevice(context) < 1700 ? 200 : 0,
          );
        return Assets.images.adorelle.path;

      case 1:
        _emptyWidget = SizedBox(
          height: DeviceUtils.isMobileDevice(context) >= 3000 ? 2000 : 
          DeviceUtils.isMobileDevice(context) >= 2500 && DeviceUtils.isMobileDevice(context) < 3000 ? 1300 : 
          DeviceUtils.isMobileDevice(context) >= 2300 && DeviceUtils.isMobileDevice(context) < 2500 ? 1000 : 
          DeviceUtils.isMobileDevice(context) >= 2000 && DeviceUtils.isMobileDevice(context) < 2300 ? 750 : 
          DeviceUtils.isMobileDevice(context) >= 1700 && DeviceUtils.isMobileDevice(context) < 2000 ? 470 :
          DeviceUtils.isMobileDevice(context) >= 1400 && DeviceUtils.isMobileDevice(context) < 1700 ? 200 : 0
          );
        return Assets.images.bd.path;

      case 2:
        _emptyWidget = SizedBox(
          height: DeviceUtils.isMobileDevice(context) >= 3000 ? 1500 : 
          DeviceUtils.isMobileDevice(context) >= 2500 && DeviceUtils.isMobileDevice(context) < 3000 ? 900 : 
          DeviceUtils.isMobileDevice(context) >= 2300 && DeviceUtils.isMobileDevice(context) < 2500 ? 750 : 
          DeviceUtils.isMobileDevice(context) >= 2000 && DeviceUtils.isMobileDevice(context) < 2300 ? 450 : 
          DeviceUtils.isMobileDevice(context) >= 1700 && DeviceUtils.isMobileDevice(context) < 2000 ? 380 :
          DeviceUtils.isMobileDevice(context) >= 1400 && DeviceUtils.isMobileDevice(context) < 1700 ? 200 : 0
          );
        return Assets.images.cuccio.path;

      case 3:
        _emptyWidget = SizedBox(
          height: DeviceUtils.isMobileDevice(context) >= 3000 ? 1800 : 
          DeviceUtils.isMobileDevice(context) >= 2500 && DeviceUtils.isMobileDevice(context) < 3000 ? 1300 : 
          DeviceUtils.isMobileDevice(context) >= 2300 && DeviceUtils.isMobileDevice(context) < 2500 ? 950 : 
          DeviceUtils.isMobileDevice(context) >= 2000 && DeviceUtils.isMobileDevice(context) < 2300 ? 650 : 
          DeviceUtils.isMobileDevice(context) >= 1700 && DeviceUtils.isMobileDevice(context) < 2000 ? 380 :
          DeviceUtils.isMobileDevice(context) >= 1400 && DeviceUtils.isMobileDevice(context) < 1700 ? 200 : 0
          );
        return Assets.images.footlogix.path;

      default:
        return Assets.images.mainbeach.path;
    }
  }
}
