import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/network/api_provider.dart';
import 'package:vnail/response/product_response.dart';

class ProductProvider extends ChangeNotifier {
  late final APIProvider _provider;
  late final BuildContext context;

  late String productTitle = tr(LocaleKeys.all_products);

  late List<Result> _allProducts = [];
  late List<Result> _currentProducts = [];
  List<Result> _recentlyViwedItems = [];

  ProductProvider(this.context) {
    _provider = APIProvider();
    setAllProducts();
  }

  Future<void> setAllProducts() async {
    _allProducts.clear();
    ProductList _webProducts = await _provider.fetchProductList(context);
    _allProducts.addAll(_webProducts.result);
    notifyListeners();
  }

  List<Result> get fetchAllProducts => _allProducts;

  List<Result> get fetchBodyCareProducts {
    _currentProducts.clear();
    for (var product in _allProducts) {
      if (product.productCategary == "Body Care") {
        _currentProducts.add(product);
      }
    }
    return _currentProducts;
  }

  List<Result> get fetchFootProducts {
    _currentProducts.clear();
    for (var product in _allProducts) {
      if (product.productCategary == "Foot") {
        _currentProducts.add(product);
      }
    }
    return _currentProducts;
  }

  List<Result> get fetchHandNailProducts {
    _currentProducts.clear();
    for (var product in _allProducts) {
      if (product.productCategary == "Hand & Nail") {
        _currentProducts.add(product);
      }
    }
    return _currentProducts;
  }

  List<Result> get fetchOtherProducts {
    _currentProducts.clear();
    for (var product in _allProducts) {
      if (product.productCategary == "Others") {
        _currentProducts.add(product);
      }
    }
    return _currentProducts;
  }

  void addRecentlyViewedItem(Result recentlyViewedItem) {
    _recentlyViwedItems.add(recentlyViewedItem);
    notifyListeners();
  }

  String currentProductTitle(int currentIndex) {
    switch (currentIndex) {
      case 0:
        return LocaleKeys.all_products;
      case 1:
        return LocaleKeys.body_care;
      case 2:
        return LocaleKeys.foot;
      case 3:
        return LocaleKeys.hand_nail;
      default:
        return LocaleKeys.others;
    }
  }

  List<Result> get getRecentlyViewedProducts => _recentlyViwedItems;
}
