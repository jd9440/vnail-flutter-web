import 'package:cool_alert/cool_alert.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:vnail/podo/userdata.dart';
import 'package:vnail/request/checkout_request.dart';
import 'package:vnail/request/login_request.dart' as logInRequest;
import 'package:vnail/request/profile_request.dart' as profileRequest;
import 'package:vnail/request/productlist_request.dart';
import 'package:vnail/response/login_response.dart';
import 'package:vnail/response/product_response.dart';
import 'package:vnail/response/profile_response.dart' as profileResponse;
import 'package:vnail/utility/DBUtils.dart';
import 'package:vnail/utility/error_handler.dart';

class APIProvider {
  final Dio _dio = Dio();

  final AUTHENTICATION_URL = 'web/session/authenticate';
  final PRODUCT_LIST_URL = 'mobile/productlist';
  final CHECKOUT_API = 'http://localhost:5000/create-checkout-session';
  final PROFILE_API = 'mobile/search/profile';

  final BaseOptions _options = BaseOptions(
    baseUrl: 'http://188.166.187.90:8069/',
    contentType: 'application/json',
    headers: {
      "Accept": "*",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Methods": "*",
    },
    responseType: ResponseType.json,
    method: '*',
  );

  APIProvider() {
    _dio.options = _options;
    if (kDebugMode) {
      _dio.interceptors.add(
        LogInterceptor(
          request: true,
          error: true,
          requestBody: true,
          responseBody: true,
        ),
      );
    }
  }

  Future<ProductList> fetchProductList(BuildContext context) async {
    try {
      Response<Map<String, dynamic>> _response = await _dio.post(
        PRODUCT_LIST_URL,
        data: productListRequestToJson(),
      );

      if (_response.data!.containsKey('error')) {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: NetworkErrorHandler.handleError(context, DioErrorType.other),
        );
      }

      return ProductList.fromJson(_response.data!);
    } on DioError catch (e) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        text: NetworkErrorHandler.handleError(context, e.type),
      );
    }
    return ProductList(jsonrpc: "2.0", result: []);
  }

  Future<void> logInUser(
      BuildContext context, String emailAddress, String password) async {
    try {
      Response<Map<String, dynamic>> _response = await _dio.post(
        AUTHENTICATION_URL,
        data: logInRequest.logInRequestToJson(
          logInRequest.LogInRequest(
            jsonrpc: "2.0",
            params:
                logInRequest.Params(login: emailAddress, password: password),
          ),
        ),
      );

      if (_response.data!.containsKey('error')) {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: NetworkErrorHandler.handleError(context, DioErrorType.other),
        );
        return;
      }

      //  updating user Data
      LogInResponse _logInResponse = LogInResponse.fromJson(_response.data!);

      Response<Map<String, dynamic>> _profileResponse = await _dio.post(
        PROFILE_API,
        data: profileRequest.profileRequestToJson(
          profileRequest.ProfileRequest(
            jsonrpc: "2.0",
            params: profileRequest.Params(
              uid: _logInResponse.result!.uid!,
            ),
          ),
        ),
      );

      if (_response.data!.containsKey('error')) {
        CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: NetworkErrorHandler.handleError(context, DioErrorType.other),
        );
        return;
      }

      profileResponse.ProfileResponse _profileResponseObject =
          profileResponse.ProfileResponse.fromJson(_profileResponse.data!);
      profileResponse.Result _result = _profileResponseObject.result!.first;

      UserData _userData = UserData(
        userLogInStatus: true,
        userCity: _result.city!,
        userCountry: _result.countryId!,
        userEmail: _result.email!,
        userId: _logInResponse.result!.uid!,
        userName: _result.name!,
        userPhone: _result.phone!,
        userState: _result.stateId!,
        userStreet1: _result.street1!,
        userStreet: _result.street!,
        userZip: _result.zip!,
        loyaltyPoints: _result.loyalty_points!,
      );

      DBUtils().setUserData(userDataToJson(_userData));
      Navigator.pop(context);
    } on DioError catch (e) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        text: NetworkErrorHandler.handleError(context, e.type),
      );
    }
  }

  Future<String> checkOutAPI(
      BuildContext context, List<CheckoutRequest> data) async {
    try {
      Dio _checkoutDioObject = new Dio();

      Response<dynamic> _response = await _checkoutDioObject.post(
        CHECKOUT_API,
        data: checkoutRequestToJson(data),
      );

      return _response.toString();
    } on DioError catch (e) {
      CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        text: NetworkErrorHandler.handleError(context, e.type),
      );
    }
    return "";
  }
}
