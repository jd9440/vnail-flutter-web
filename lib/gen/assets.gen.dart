/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

import 'package:flutter/widgets.dart';

class $AssetsImagesGen {
  const $AssetsImagesGen();

  AssetGenImage get about1 => const AssetGenImage('assets/images/about1.webp');
  AssetGenImage get about2 => const AssetGenImage('assets/images/about2.webp');
  AssetGenImage get about3 => const AssetGenImage('assets/images/about3.webp');
  AssetGenImage get aboutusbg =>
      const AssetGenImage('assets/images/aboutusbg.webp');
  AssetGenImage get addedProducts =>
      const AssetGenImage('assets/images/added_products.webp');
  AssetGenImage get adorelle =>
      const AssetGenImage('assets/images/adorelle.webp');
  AssetGenImage get bd => const AssetGenImage('assets/images/bd.webp');
  AssetGenImage get bodyCareProduct =>
      const AssetGenImage('assets/images/body_care_product.webp');
  AssetGenImage get cuccio => const AssetGenImage('assets/images/cuccio.webp');
  AssetGenImage get deliveryDisclaimer =>
      const AssetGenImage('assets/images/delivery_disclaimer.webp');
  AssetGenImage get deliveryMo2 =>
      const AssetGenImage('assets/images/delivery_mo2.webp');
  AssetGenImage get deliveryRp =>
      const AssetGenImage('assets/images/delivery_rp.webp');
  AssetGenImage get deliveryTc =>
      const AssetGenImage('assets/images/delivery_tc.webp');
  AssetGenImage get designerBg =>
      const AssetGenImage('assets/images/designer_bg.webp');
  AssetGenImage get footProduct =>
      const AssetGenImage('assets/images/foot_product.webp');
  AssetGenImage get footlogix =>
      const AssetGenImage('assets/images/footlogix.webp');
  AssetGenImage get gallery =>
      const AssetGenImage('assets/images/gallery.webp');
  AssetGenImage get handNailProduct =>
      const AssetGenImage('assets/images/hand_nail_product.webp');
  AssetGenImage get logo => const AssetGenImage('assets/images/logo.webp');
  AssetGenImage get mainbeach =>
      const AssetGenImage('assets/images/mainbeach.webp');
  AssetGenImage get manicure =>
      const AssetGenImage('assets/images/manicure.webp');
  AssetGenImage get manicureData =>
      const AssetGenImage('assets/images/manicure_data.webp');
  AssetGenImage get manicureService =>
      const AssetGenImage('assets/images/manicure_service.webp');
  AssetGenImage get memberBox =>
      const AssetGenImage('assets/images/member_box.webp');
  AssetGenImage get memberOffer =>
      const AssetGenImage('assets/images/member_offer.webp');
  AssetGenImage get nails1 => const AssetGenImage('assets/images/nails1.webp');
  AssetGenImage get nails2 => const AssetGenImage('assets/images/nails2.webp');
  AssetGenImage get nails3 => const AssetGenImage('assets/images/nails3.webp');
  AssetGenImage get otherProduct =>
      const AssetGenImage('assets/images/other_product.webp');
  AssetGenImage get ourBrand =>
      const AssetGenImage('assets/images/our_brand.webp');
  AssetGenImage get painting =>
      const AssetGenImage('assets/images/painting.webp');
  AssetGenImage get pedicureData =>
      const AssetGenImage('assets/images/pedicure_data.webp');
  AssetGenImage get pedicureService =>
      const AssetGenImage('assets/images/pedicure_service.webp');
  AssetGenImage get productBgImage =>
      const AssetGenImage('assets/images/product_bg_image.webp');
  AssetGenImage get products =>
      const AssetGenImage('assets/images/products.webp');
  AssetGenImage get productsBg =>
      const AssetGenImage('assets/images/products_bg.webp');
  AssetGenImage get serviceBg =>
      const AssetGenImage('assets/images/service_bg.webp');
  AssetGenImage get simple1 =>
      const AssetGenImage('assets/images/simple1.webp');
  AssetGenImage get simple2 =>
      const AssetGenImage('assets/images/simple2.webp');
  AssetGenImage get simple3 =>
      const AssetGenImage('assets/images/simple3.webp');
  AssetGenImage get simple4 =>
      const AssetGenImage('assets/images/simple4.webp');
  AssetGenImage get simple5 =>
      const AssetGenImage('assets/images/simple5.webp');
  AssetGenImage get treatment =>
      const AssetGenImage('assets/images/treatment.webp');
  AssetGenImage get treatmentData =>
      const AssetGenImage('assets/images/treatment_data.webp');
  AssetGenImage get vnailBeauty =>
      const AssetGenImage('assets/images/vnail_beauty.webp');
  AssetGenImage get vnailBg =>
      const AssetGenImage('assets/images/vnail_bg.webp');
  AssetGenImage get whatsappIcon =>
      const AssetGenImage('assets/images/whatsapp_icon.webp');
}

class $AssetsTranslationsGen {
  const $AssetsTranslationsGen();

  String get en => 'assets/translations/en.json';
  String get zh => 'assets/translations/zh.json';
}

class Assets {
  Assets._();

  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsTranslationsGen translations = $AssetsTranslationsGen();
}

class AssetGenImage extends AssetImage {
  const AssetGenImage(String assetName) : super(assetName);

  Image image({
    Key? key,
    ImageFrameBuilder? frameBuilder,
    ImageLoadingBuilder? loadingBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? width,
    double? height,
    Color? color,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
  }) {
    return Image(
      key: key,
      image: this,
      frameBuilder: frameBuilder,
      loadingBuilder: loadingBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      filterQuality: filterQuality,
    );
  }

  String get path => assetName;
}
