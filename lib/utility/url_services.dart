import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vnail/utility/alertdialog.dart';

class URLServiceUtils {
  final BuildContext context;

  URLServiceUtils(this.context);

  Future<void> openWhatsApp() async {
    return await _launchURL('https://api.whatsapp.com/send?phone=+85268900864');
  }

  Future<void> openPhoneNumber() async {
    return await _launchURL('tel://+85268900864');
  }

  Future<void> openEmail() async {
    return await _launchURL('mailto:vnail00829@gmail.com');
  }

  Future<void> openFacebookPage() async {
    return await _launchURL('https://www.facebook.com/vnailbeauty.hk/');
  }

  Future<void> openInstagramPage() async {
    return await _launchURL(
        'https://instagram.com/vnailbeauty?utm_medium=copy_link');
  }

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      await AlertDialogUtils.showFailureDialog(context);
    }
  }
}
