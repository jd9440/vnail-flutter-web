import 'package:flutter/material.dart';

class Routing {
  static const String AboutRoute = '/about';
  static const String BrandsRoute = '/brands';
  static const String GalleryRoute = '/gallery';
  static const String HomeRoute = '/index.html';
  static const String MemberOfferRoute = '/memberOffer';
  static const String ProductsRoute = '/products';
  static const String ProductDescriptionRoute = '/productDescription';
  static const String ProfileRoute = '/profile';
  static const String ServicesRoute = '/services';
  static const String TermsRoute = '/terms';
  static const String WishlistRoute = '/wishlist';

  static Route<dynamic> pushNamedPage(
      RouteSettings settings, WidgetBuilder page,
      {int duration = 300}) {
    return PageRouteBuilder(
      settings: settings,
      transitionDuration: Duration(milliseconds: duration),
      pageBuilder: (context, animation, secondaryAnimation) => page(context),
    );
  }
}
