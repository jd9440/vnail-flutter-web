import 'package:hive_flutter/hive_flutter.dart';
import 'package:vnail/podo/userdata.dart';

class DBUtils {
  static final String userKey = 'UserKey';
  static final String userBox = 'userBox';

  Box _userDataBox = Hive.box<String>(userBox);

  UserData getUserData() {
    return userDataFromJson(_userDataBox.get(
      userKey,
      defaultValue: userDataToJson(
        UserData(),
      ),
    )!);
  }

  void setUserData(String userData) {
    _userDataBox.put(
      userKey,
      userData,
    );
  }

  Future<int> clearAllDataAndLogOut() async {
    return await _userDataBox.clear();
  }
}
