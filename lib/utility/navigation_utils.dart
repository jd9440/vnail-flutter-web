import 'package:flutter/material.dart';

class NavigationUtils {
  final BuildContext context;

  NavigationUtils(this.context);

  Future<void> navigateToPage(String routeName, {dynamic args}) {
    return Navigator.pushNamed(context, routeName, arguments: args);
  }
}
