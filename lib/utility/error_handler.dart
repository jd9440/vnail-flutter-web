import 'package:cool_alert/cool_alert.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vnail/generated/locale_keys.g.dart';

class NetworkErrorHandler {
  void showAlertDialog(BuildContext context, String str) async {
    await CoolAlert.show(
      context: context,
      type: CoolAlertType.error,
      text: str,
    );
  }

  static String handleError(BuildContext context, DioErrorType type) {
    switch (type) {
      case DioErrorType.cancel:
        return tr(LocaleKeys.request_cancelled);
      case DioErrorType.connectTimeout:
        return tr(LocaleKeys.request_connection_timeout);
      case DioErrorType.sendTimeout:
        return tr(LocaleKeys.request_send_timeout);
      case DioErrorType.receiveTimeout:
        return tr(LocaleKeys.request_receive_timeout);
      case DioErrorType.response:
        return tr(LocaleKeys.request_server_error);
      default:
        return tr(LocaleKeys.request_default_error);
    }
  }
}
