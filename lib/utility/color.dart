import 'package:flutter/material.dart';

class VNailColors {
  static final Color mainBgColor = Color(0xFFE1D8DD);
  static final Color colorStyle4 = Color(0xFF825C5D);
  static final Color serviceTextColor = Color(0xFF562C2E);
  static final Color colorStyle3 = Color(0xFFC6ACAD);
  static final Color bookNowButtonColor = Color(0xFF64B161);
  static final Color colorStyle5 = Color(0xFF572D2F);
  static final Color colorStyle2 = Colors.white;
  static final Color footerColor = Color(0xFFE0D7DC);
}
