import 'dart:ui';

import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hovering/hovering.dart';
import 'package:vnail/utility/color.dart';

class TextWidget {
  static Widget titleText(String text,
      {VoidCallback? onClickAction,
      bool isUpperCase = false,
      double? fontSize,
      FontWeight? fontWeight,
      Color? textColor}) {
    return AutoSizeText(
      (isUpperCase) ? tr(text).toUpperCase() : tr(text),
      style: GoogleFonts.playfairDisplay(
        fontSize: fontSize ?? 64,
        fontWeight: fontWeight ?? FontWeight.w700,
        color: textColor ?? Colors.white,
      ),
    );
  }

  static Widget serviceTitleText(
    String text, {
    Color? textColor,
  }) {
    return AutoSizeText(
      tr(text),
      style: GoogleFonts.playfairDisplay(
        fontSize: 64,
        fontWeight: FontWeight.w400,
        color: textColor ?? VNailColors.serviceTextColor,
      ),
    );
  }

  static Widget serviceDescriptionText(String text,
      {double? fontSize,
      FontWeight? fontWeight,
      Color? textColor,
      TextDecoration? textDecoration,
      int? maxLines,
      VoidCallback? onClickAction}) {
    return TextButton(
      onPressed: onClickAction,
      child: Text(
        tr(text),
        softWrap: true,
        maxLines: maxLines ?? 1,
        style: GoogleFonts.roboto(
          fontSize: fontSize ?? 18,
          fontWeight: fontWeight ?? FontWeight.w400,
          color: textColor ?? VNailColors.serviceTextColor,
          decoration: textDecoration ?? TextDecoration.none,
        ),
      ),
    );
  }

  static Widget serviceNameText(String text,
      {Color? fontColor, VoidCallback? onClickAction}) {
    return Text(
      tr(text),
      style: GoogleFonts.playfairDisplay(
        fontSize: 30,
        fontWeight: FontWeight.w400,
        color: fontColor ?? VNailColors.serviceTextColor,
      ),
    );
  }

  static Widget serviceButtonText(String text, {VoidCallback? onClickAction}) {
    return AutoSizeText(
      tr(text),
      style: GoogleFonts.playfairDisplay(
        fontSize: 18,
        fontWeight: FontWeight.w500,
        color: Colors.white,
      ),
    );
  }

  static Widget subTitleText(String text, {VoidCallback? onClickAction}) {
    return AutoSizeText(
      tr(text),
      style: GoogleFonts.roboto(
        fontSize: 23.4,
        letterSpacing: 8,
        fontWeight: FontWeight.w400,
        color: Colors.white,
      ),
    );
  }

  static Widget footerTextStyle(
    String text, {
    double? fontSize,
    FontWeight? fontWeight,
    VoidCallback? onClickAction,
  }) {
    return HoverCrossFadeWidget(
      firstChild: TextButton(
        onPressed: onClickAction,
        child: Text(
          tr(text),
          style: GoogleFonts.roboto(
            fontSize: fontSize ?? 16,
            fontWeight: fontWeight ?? FontWeight.w400,
            color: VNailColors.colorStyle4,
          ),
        ),
      ),
      duration: Duration(milliseconds: 1),
      secondChild: TextButton(
        onPressed: onClickAction,
        child: Text(
          tr(text),
          style: GoogleFonts.roboto(
            fontSize: fontSize ?? 16,
            fontWeight: fontWeight ?? FontWeight.w400,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  static Widget footerTextWithIconStyle(String text, IconData iconData,
      {VoidCallback? onClickAction}) {
    return HoverCrossFadeWidget(
      firstChild: TextButton.icon(
        icon: FaIcon(
          iconData,
          color: VNailColors.colorStyle4,
        ),
        label: Text(
          tr(text),
          style: GoogleFonts.roboto(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: VNailColors.colorStyle4,
          ),
        ),
        onPressed: onClickAction,
      ),
      duration: Duration(milliseconds: 1),
      secondChild: TextButton.icon(
        icon: FaIcon(
          iconData,
          color: Colors.white,
        ),
        label: Text(
          tr(text),
          style: GoogleFonts.roboto(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: Colors.white,
          ),
        ),
        onPressed: onClickAction,
      ),
    );
  }

  static Widget footerIconStyle(IconData iconData,
      {VoidCallback? onClickAction}) {
    return HoverCrossFadeWidget(
      firstChild: IconButton(
        icon: FaIcon(
          iconData,
          color: VNailColors.colorStyle4,
        ),
        onPressed: onClickAction,
      ),
      duration: Duration(milliseconds: 1),
      secondChild: IconButton(
        icon: FaIcon(
          iconData,
          color: Colors.white,
        ),
        onPressed: onClickAction,
      ),
    );
  }

  static Widget roboto400Normal(String text,
      {double? fontSize,
      FontWeight? fontWeight,
      Color? textColor,
      TextDecoration? textDecoration,
      int? maxLines}) {
    return AutoSizeText(
      tr(text),
      softWrap: true,
      style: GoogleFonts.roboto(
        fontSize: fontSize ?? 18,
        fontWeight: fontWeight ?? FontWeight.w400,
        color: textColor ?? VNailColors.serviceTextColor,
        decoration: textDecoration ?? TextDecoration.none,
      ),
    );
  }

  static Widget productDescription(String text,
      {double? fontSize,
      FontWeight? fontWeight,
      Color? textColor,
      TextDecoration? textDecoration,
      bool? isUpperCase,
      int? maxLines}) {
    return AutoSizeText(
      (isUpperCase ?? false) ? text : text.toUpperCase(),
      softWrap: true,
      maxLines: maxLines ?? 1,
      overflow: TextOverflow.ellipsis,
      style: GoogleFonts.roboto(
        fontSize: fontSize ?? 18,
        fontWeight: fontWeight ?? FontWeight.w400,
        color: textColor ?? VNailColors.serviceTextColor,
        decoration: textDecoration ?? TextDecoration.none,
      ),
    );
  }
}
