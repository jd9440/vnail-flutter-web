import 'package:easy_localization/easy_localization.dart';
import 'package:vnail/generated/locale_keys.g.dart';

class ValidateUtils {
  static String? isValidEmail(String? email) {
    String? errorTxt = isEmptyCheck(email);

    if (errorTxt?.isNotEmpty ?? false) {
      errorTxt = tr(LocaleKeys.required_field);
      return errorTxt;
    }

    if (!RegExp(r"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$").hasMatch(email!)) {
      errorTxt = tr(LocaleKeys.invalid_email);
      return errorTxt;
    }

    return null;
  }

  static String? isValidMobile(String phoneNumber) {
    String? errorTxt = isEmptyCheck(phoneNumber);

    if (!RegExp(r"^[0-9]{10}$").hasMatch(phoneNumber)) {
      errorTxt = tr(LocaleKeys.invalid_phoneNumber);
    }

    return errorTxt;
  }

  //  * use this one for empty check, but returns empty string
  static String? isEmptyCheck(String? value) {
    if (["", null, false, 0].contains(value)) {
      return tr(LocaleKeys.required_field);
    }

    return null;
  }

  //  * use this one for empty check, because we need to return null for the required parameter...
  static String? isRequiredCheck(String? value) {
    if (["", null, false, 0].contains(value)) {
      return tr(LocaleKeys.required_field);
    }

    return null;
  }

  static String? isValidOTP(String otp) {
    String? errorTxt = isEmptyCheck(otp);

    if (!RegExp(r"^[0-9]{6}$").hasMatch(otp)) {
      errorTxt = tr(LocaleKeys.invalid_otp);
    }

    return errorTxt;
  }
}
