import 'package:cool_alert/cool_alert.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vnail/generated/locale_keys.g.dart';

class AlertDialogUtils {
  static Future<void> showFailureDialog(BuildContext context) async {
    await CoolAlert.show(
      context: context,
      type: CoolAlertType.error,
      text: tr(LocaleKeys.failure_application_msg),
      animType: CoolAlertAnimType.rotate,
      loopAnimation: true,
    );
  }
}
