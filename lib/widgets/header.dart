import 'package:cool_alert/cool_alert.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/podo/userdata.dart';
import 'package:vnail/providers/cart_provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/DBUtils.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/widgets/aboutus.dart';
import 'package:vnail/widgets/brands.dart';
import 'package:vnail/widgets/gallery.dart';
import 'package:vnail/widgets/header_text.dart';
import 'package:vnail/widgets/homecontent.dart';
import 'package:vnail/widgets/member_offer.dart';
import 'package:vnail/widgets/products.dart';
import 'package:vnail/widgets/profile.dart';
import 'package:vnail/widgets/services.dart';
import 'package:vnail/widgets/welcome_cart.dart';
import 'package:vnail/widgets/wishlist.dart';

class HeaderWidget extends StatefulWidget {
  const HeaderWidget({
    Key? key,
  }) : super(key: key);

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  late TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: VNailColors.mainBgColor,
      width: MediaQueryUtils(context).width,
      padding: EdgeInsets.only(left: 24, right: 24),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        rowCrossAxisAlignment: CrossAxisAlignment.end,
        children: [
          ResponsiveRowColumnItem(
              child: Image(
            image: Assets.images.logo,
            height: 100,
            width: 100,
          )),
          ResponsiveRowColumnItem(
            child: HeaderContentWidget(),
          ),
        ],
      ),
    );
  }
}

class HeaderContentWidget extends StatelessWidget {
  const HeaderContentWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _homePageProvider = context.read<HomePageProvider>();
    return ValueListenableBuilder<Box<String>>(
      valueListenable: Hive.box<String>(DBUtils.userBox).listenable(),
      builder: (context, value, child) {
        String _stringData = value.get(DBUtils.userKey,
            defaultValue: userDataToJson(UserData()))!;
        UserData _data = userDataFromJson(_stringData);
        final bool _isUserLoggedIn = _data.userLogInStatus;
        final bool _visibility = (_isUserLoggedIn) ? true : false;
        return ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          columnCrossAxisAlignment: CrossAxisAlignment.end,
          columnMainAxisAlignment: MainAxisAlignment.start,
          children: [
            ResponsiveRowColumnItem(
              child: SizedBox(
                height: 8,
              ),
            ),
            ResponsiveRowColumnItem(
                child: Padding(
              padding: const EdgeInsets.only(left: 24, right: 36),
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                rowMainAxisSize: MainAxisSize.min,
                rowMainAxisAlignment: MainAxisAlignment.end,
                rowCrossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ResponsiveRowColumnItem(
                    child: Builder(
                      builder: (context) => Flexible(
                        flex: 1,
                        child: HeaderTextWidget(
                          (_isUserLoggedIn)
                              ? _data.userName
                              : LocaleKeys.signin_header_txt,
                          (_isUserLoggedIn)
                              ? () => null
                              : () async => await _homePageProvider
                                  .showLogInAlertDialog(context),
                        ),
                      ),
                    ),
                  ),
                  ResponsiveRowColumnItem(
                    child: Flexible(
                      flex: 1,
                      child: Visibility(
                        visible: _visibility,
                        child: ProfileButton(),
                      ),
                    ),
                  ),
                  ResponsiveRowColumnItem(
                    child: Flexible(
                      flex: 1,
                      child: FavouriteButton(),
                    ),
                  ),
                  ResponsiveRowColumnItem(
                    child: Flexible(
                      flex: 1,
                      child: AddToCartButton(),
                    ),
                  ),
                ],
              ),
            )),
            ResponsiveRowColumnItem(
              child: SizedBox(
                height: 16,
              ),
            ),
            ResponsiveRowColumnItem(
                child: Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                rowMainAxisAlignment: MainAxisAlignment.end,
                rowCrossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.home,
                    () => _homePageProvider.updateHomeContent(HomeContent()),
                  )),
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.service_header_txt,
                    () =>
                        _homePageProvider.updateHomeContent(ServiceContent(0)),
                  )),
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.product_header_txt,
                    () =>
                        _homePageProvider.updateHomeContent(ProductContent(0)),
                  )),
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.brand_header_txt,
                    () => _homePageProvider.updateHomeContent(Brands()),
                  )),
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.gallery_header_txt,
                    () => _homePageProvider.updateHomeContent(GalleryWidget()),
                  )),
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.about_header_txt,
                    () => _homePageProvider.updateHomeContent(AboutUsWidget()),
                  )),
                  ResponsiveRowColumnItem(
                      child: HeaderTextWidget(
                    LocaleKeys.join_header_txt,
                    () => _homePageProvider.updateHomeContent(MemberOffer()),
                  )),
                ],
              ),
            )),
            ResponsiveRowColumnItem(
                child: SizedBox(
              height: 8,
            )),
          ],
        );
      },
    );
  }
}

class AddToCartButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _cartProvider = context.read<CartProvider>();
    final _homePageProvider = context.read<HomePageProvider>();
    return InkWell(
      onTap: () => (_cartProvider.ifCartEmpty())
          ? CoolAlert.show(
              context: context,
              type: CoolAlertType.info,
              text: tr(LocaleKeys.cart_empty_msg))
          : _homePageProvider.updateHomeContent(
              WelcomeCart(PaymentFirstPage()),
            ),
      child: Image(
        image: Assets.images.addedProducts,
        height: 21,
        width: 21,
        color: VNailColors.colorStyle4,
        fit: BoxFit.fill,
      ),
    );
  }
}

class FavouriteButton extends StatelessWidget {
  const FavouriteButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _homePageProvider = context.read<HomePageProvider>();
    return IconButton(
      icon: Icon(
        Icons.favorite_rounded,
        size: 21,
        color: VNailColors.colorStyle4,
      ),
      onPressed: () => _homePageProvider.updateHomeContent(WishlistWidget()),
    );
  }
}

class SearchWidget extends StatelessWidget {
  const SearchWidget({
    Key? key,
    required TextEditingController controller,
  })  : _controller = controller,
        super(key: key);

  final TextEditingController _controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      width: 140,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(28),
        ),
      ),
      padding: EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
      alignment: Alignment.center,
      child: TypeAheadField(
        textFieldConfiguration: TextFieldConfiguration(
          textAlign: TextAlign.center,
          controller: _controller,
          keyboardType: TextInputType.text,
          maxLines: 1,
          minLines: 1,
          decoration: InputDecoration(
            hintText: tr(LocaleKeys.search_header_txt),
            hintMaxLines: 1,
            hintStyle: GoogleFonts.roboto(
              fontSize: 18,
              color: Color.fromRGBO(130, 92, 93, 1),
            ),
            border: InputBorder.none,
            errorBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            focusedErrorBorder: InputBorder.none,
          ),
        ),
        itemBuilder: (BuildContext context, itemData) {
          return Text("Sample Data");
        },
        onSuggestionSelected: (Object? suggestion) {},
        suggestionsCallback: (String pattern) {
          return List.generate(10, (index) => Text("data"));
        },
      ),
    );
  }
}

class ProfileButton extends StatelessWidget {
  const ProfileButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _homePageProvider = context.read<HomePageProvider>();
    return IconButton(
      icon: Icon(
        Icons.person_outline_rounded,
        size: 24,
        color: VNailColors.colorStyle4,
      ),
      onPressed: () => _homePageProvider.updateHomeContent(ProfileWidget()),
    );
  }
}
