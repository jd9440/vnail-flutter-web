import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/utility/validation.dart';
import 'package:vnail/widgets/textinput.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:provider/provider.dart';
import 'package:vnail/utility/mediaquery.dart';

class SignUpWidget extends StatefulWidget {
  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends State<SignUpWidget> {
  late final TextEditingController _nameController,
      _mobileController,
      _passwordController,
      _confirmPasswordController;

  late final FocusNode _nameNode,
      _mobileNode,
      _passwordNode,
      _confirmPasswordNode;

  @override
  void initState() {
    _nameController = TextEditingController();
    _mobileController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();

    _nameNode = FocusNode();
    _mobileNode = FocusNode();
    _passwordNode = FocusNode();
    _confirmPasswordNode = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _mobileController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    _nameNode.dispose();
    _mobileNode.dispose();
    _passwordNode.dispose();
    _confirmPasswordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<HomePageProvider>();
    return Container(
      // width: MediaQueryUtils(context).width * 0.35,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, left: 16, bottom: 24),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextWidget.serviceNameText(LocaleKeys.sign_up),
                IconButton(
                  icon: Icon(
                    Icons.close,
                    color: VNailColors.colorStyle4,
                    size: 16,
                  ),
                  onPressed: () => _provider.popDialog(context),
                ),
              ],
            ),
          ),
          TextInputWidget(
            TextInputType.text,
            controller: _nameController,
            focusNode: _nameNode,
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            maxLength: 10,
            maxLines: 1,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.name_hint),
            onFieldSubmittedAction: (data) =>
                ValidateUtils.isRequiredCheck(data),
          ),
          TextInputWidget(
            TextInputType.phone,
            controller: _mobileController,
            focusNode: _mobileNode,
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            maxLength: 10,
            maxLines: 1,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.mobile_number_hint),
            onFieldSubmittedAction: (data) => ValidateUtils.isValidMobile(data),
          ),
          TextInputWidget(
            TextInputType.text,
            focusNode: _passwordNode,
            controller: _passwordController,
            maxLength: 24,
            maxLines: 1,
            onFieldSubmittedAction: (data) =>
                ValidateUtils.isRequiredCheck(data),
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.password_hint),
            errorText: tr(LocaleKeys.required_field),
            obscureText: _provider.obscureText,
            suffixIcon: (_provider.obscureText)
                ? IconButton(
                    iconSize: 18,
                    icon: Icon(
                      Icons.visibility_off,
                      color: VNailColors.colorStyle4,
                    ),
                    onPressed: () => _provider.toggleObscureValue(),
                  )
                : IconButton(
                    iconSize: 18,
                    icon: Icon(
                      Icons.visibility,
                      color: VNailColors.colorStyle4,
                    ),
                    onPressed: () => _provider.toggleObscureValue(),
                  ),
          ),
          TextInputWidget(
            TextInputType.text,
            focusNode: _confirmPasswordNode,
            controller: _confirmPasswordController,
            maxLength: 24,
            maxLines: 1,
            onFieldSubmittedAction: (data) =>
                ValidateUtils.isRequiredCheck(data),
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.confirmpassword_hint),
            errorText: tr(LocaleKeys.required_field),
            obscureText: false,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Checkbox(
                value: _provider.signUpCheckBoxValue,
                onChanged: (value) => _provider.toggleSignUpValue(),
                checkColor: VNailColors.colorStyle5,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: TextWidget.serviceDescriptionText(
                    LocaleKeys.account_agreement,
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    maxLines: 2,
                    textColor: VNailColors.colorStyle4,
                  ),
                ),
              ),
            ],
          ),
          TextButton(
            onPressed: () {
              //  TODO : Implement log in button
            },
            child: Container(
              width: MediaQueryUtils(context).width * 0.2,
              height: MediaQueryUtils(context).width * 0.035,
              margin: EdgeInsets.only(left: 16, right: 16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: VNailColors.colorStyle5,
              ),
              child: TextWidget.serviceDescriptionText(
                LocaleKeys.sign_up,
                fontSize: 12,
                fontWeight: FontWeight.w500,
                textColor: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextWidget.serviceDescriptionText(
                  LocaleKeys.account_existence,
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
                TextWidget.serviceDescriptionText(
                  LocaleKeys.signin,
                  fontWeight: FontWeight.w700,
                  onClickAction: () => _provider.changeIndex(0),
                  fontSize: 18,
                  textDecoration: TextDecoration.underline,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
