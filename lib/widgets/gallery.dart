import 'package:flutter/material.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/pageimage.dart';

import 'circularimage.dart';

class GalleryWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQueryUtils(context).width,
      decoration: BoxDecoration(
        color: VNailColors.mainBgColor,
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          PageImage(
            LocaleKeys.gallery_header_txt,
            Assets.images.gallery,
          ),
          SizedBox(
            height: 36,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(36, 0, 36, 0),
            child: TextWidget.titleText(
              LocaleKeys.simple,
              textColor: VNailColors.colorStyle4,
            ),
          ),
          SizedBox(
            height: 36,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(36, 0, 36, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: CircularImage(32, Assets.images.simple1),
                  flex: 1,
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 16,
                      mainAxisSpacing: 16,
                      childAspectRatio: 1.1,
                    ),
                    itemBuilder: (context, index) {
                      switch (index) {
                        case 0:
                          return CircularImage(32, Assets.images.simple2);
                        case 1:
                          return CircularImage(32, Assets.images.simple3);
                        case 2:
                          return CircularImage(32, Assets.images.simple4);
                        default:
                          return CircularImage(32, Assets.images.simple5);
                      }
                    },
                    itemCount: 4,
                  ),
                  flex: 1,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 36,
          ),
          TextWidget.titleText(
            LocaleKeys.painting,
            textColor: VNailColors.colorStyle4,
          ),
          SizedBox(
            height: 36,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(36, 0, 36, 128),
            child: Image.asset(
              Assets.images.painting.path,
              height: MediaQueryUtils(context).height >= 1200
                  ? MediaQueryUtils(context).height / 2
                  : MediaQueryUtils(context).height * 1.1,
              width: MediaQueryUtils(context).width >= 1200
                  ? MediaQueryUtils(context).width / 0.5
                  : MediaQueryUtils(context).width * 5,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 48,
          ),
        ],
      ),
    );
  }
}
