import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/utility/validation.dart';
import 'package:vnail/widgets/textinput.dart';

class ForgotPasswordWidget extends StatefulWidget {
  @override
  _ForgotPasswordWidgetState createState() => _ForgotPasswordWidgetState();
}

class _ForgotPasswordWidgetState extends State<ForgotPasswordWidget> {
  late final TextEditingController _mobileController,
      _passwordController,
      _confirmPasswordController;

  late final FocusNode _mobileNode, _passwordNode, _confirmPasswordNode;

  @override
  void initState() {
    _mobileController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();

    _mobileNode = FocusNode();
    _passwordNode = FocusNode();
    _confirmPasswordNode = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    _mobileController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    _mobileNode.dispose();
    _passwordNode.dispose();
    _confirmPasswordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _provider = context.read<HomePageProvider>();
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, left: 16, bottom: 24),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextWidget.serviceNameText(LocaleKeys.forgot_password_question),
                IconButton(
                  icon: Icon(
                    Icons.close,
                    color: VNailColors.colorStyle4,
                    size: 16,
                  ),
                  onPressed: () => _provider.popDialog(context),
                ),
              ],
            ),
          ),
          SizedBox(
            height: MediaQueryUtils(context).height * 0.1,
          ),
          TextInputWidget(
            TextInputType.text,
            controller: _mobileController,
            focusNode: _mobileNode,
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            maxLength: 10,
            maxLines: 1,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.mobile_number_hint),
            onFieldSubmittedAction: (data) => ValidateUtils.isValidMobile(data),
          ),
          TextInputWidget(
            TextInputType.text,
            controller: _passwordController,
            focusNode: _passwordNode,
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            maxLength: 10,
            maxLines: 1,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.password_hint),
            onFieldSubmittedAction: (data) =>
                ValidateUtils.isRequiredCheck(data),
          ),
          TextInputWidget(
            TextInputType.text,
            controller: _confirmPasswordController,
            focusNode: _confirmPasswordNode,
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            maxLength: 10,
            maxLines: 1,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.confirmpassword_hint),
            onFieldSubmittedAction: (data) =>
                ValidateUtils.isRequiredCheck(data),
          ),
          SizedBox(
            height: MediaQueryUtils(context).height * 0.1,
          ),
          TextButton(
            onPressed: () {
              //  TODO : Implement log in button
            },
            child: Container(
              width: MediaQueryUtils(context).width * 0.2,
              height: MediaQueryUtils(context).width * 0.035,
              margin: EdgeInsets.only(left: 16, right: 16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: VNailColors.colorStyle5,
              ),
              child: TextWidget.serviceDescriptionText(
                LocaleKeys.change_password,
                fontSize: 12,
                fontWeight: FontWeight.w500,
                textColor: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
