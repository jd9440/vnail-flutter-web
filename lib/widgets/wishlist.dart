import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/cart_provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/providers/wishlist_provider.dart';
import 'package:vnail/response/product_response.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/device_utils.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/custom_widget.dart';
import 'package:vnail/widgets/welcome_cart.dart';

class WishlistWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _homeProvider = context.read<HomePageProvider>();
    final _cartProvider = context.read<CartProvider>();
    final _wishlistProvider = context.read<WishListProvider>();
    return Container(
      padding: EdgeInsets.only(left: 72, top: 80, right: 72, bottom: 40),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Assets.images.productsBg,
          fit: BoxFit.cover,
        ),
      ),
      height: MediaQueryUtils(context).height,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextWidget.titleText(
            LocaleKeys.wishlist,
            textColor: VNailColors.colorStyle4,
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            flex: 1,
            child: WishlistItems(),
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomButton(
                LocaleKeys.checkout,
                Icons.shopping_bag_outlined,
                () {
                  _cartProvider
                      .addWishlistToCartItems(_wishlistProvider.wishListItems);
                  _homeProvider.updateHomeContent(
                    WelcomeCart(PaymentFirstPage()),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class WishlistItems extends StatefulWidget {
  const WishlistItems({
    Key? key,
  }) : super(key: key);

  @override
  State<WishlistItems> createState() => _WishlistItemsState();
}

class _WishlistItemsState extends State<WishlistItems> {
  @override
  Widget build(BuildContext context) {
    final _wishlistProvider = context.watch<WishListProvider>();
    final _cartProvider = context.watch<CartProvider>();
    final _list = _wishlistProvider.wishListItems;
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        Result _individualItem = _list[index];
        bool _isItemInTheCart =
            _cartProvider.checkIfAlreadyExists(_individualItem);
        return WishlistItem(
          _individualItem.name,
          _individualItem.price.toString(),
          _isItemInTheCart,
          () => _wishlistProvider.removeProductFromWishList(_individualItem),
          () => (_isItemInTheCart)
              ? _cartProvider.removeProductFromCart(_individualItem)
              : _cartProvider.addProductToCart(_individualItem),
        );
      },
      itemCount: _wishlistProvider.wishListItems.length,
    );
  }
}

class WishlistItem extends StatelessWidget {
  final String itemName, itemPrice;
  final bool isAddedToCart;
  final VoidCallback removeButtonClick, addToCartClick;

  WishlistItem(
    this.itemName,
    this.itemPrice,
    this.isAddedToCart,
    this.removeButtonClick,
    this.addToCartClick,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQueryUtils(context).width,
      height: DeviceUtils.isMobileDevice(context) > 1100 ? 200 : MediaQueryUtils(context).height * 0.3,
      margin: EdgeInsets.only(bottom: 24),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ]),
            child: Image(
              image: Assets.images.bodyCareProduct,
              fit: BoxFit.contain,
              height: 260,
              width: 248,
            ),
          ),
          SizedBox(
            width: 48,
          ),
          Flex(
            direction: Axis.vertical,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Flexible(
                flex: 2,
                child: TextWidget.productDescription(
                  itemName,
                  fontSize: 24,
                  textColor: VNailColors.colorStyle4,
                ),
              ),
              SizedBox(height: DeviceUtils.isMobileDevice(context) > 2000 ? 0 : 5),
              Flexible(
                flex: 2,
                child: TextWidget.productDescription(
                  '$itemPrice \$',
                  fontSize: 24,
                ),
              ),
              SizedBox(height: DeviceUtils.isMobileDevice(context) > 2000 ? 0 : 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  CustomButton(
                    LocaleKeys.add_to_cart,
                    (isAddedToCart)
                        ? Icons.shopping_cart
                        : Icons.shopping_cart_outlined,
                    addToCartClick,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  CustomButton(
                    LocaleKeys.remove,
                    Icons.delete_outline,
                    removeButtonClick,
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}

