import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vnail/utility/color.dart';

class HeaderTextWidget extends StatefulWidget {
  final VoidCallback onClickAction;
  final String text;

  HeaderTextWidget(this.text, this.onClickAction);

  @override
  _HeaderTextWidgetState createState() => _HeaderTextWidgetState();
}

class _HeaderTextWidgetState extends State<HeaderTextWidget> {
  Color _backGroundColor = VNailColors.mainBgColor,
      _textColor = VNailColors.colorStyle4;

  _toggleOnEnterColors() {
    _backGroundColor = VNailColors.colorStyle5;
    _textColor = Colors.white;
    setState(() {});
  }

  _toggleOnExitColors() {
    _textColor = VNailColors.colorStyle4;
    _backGroundColor = VNailColors.mainBgColor;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
      child: MouseRegion(
        onEnter: (event) => _toggleOnEnterColors(),
        onExit: (event) => _toggleOnExitColors(),
        child: TextButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.resolveWith(
                (states) => EdgeInsets.all(16)),
            backgroundColor:
                MaterialStateProperty.resolveWith((states) => _backGroundColor),
          ),
          onPressed: widget.onClickAction,
          child: Text(
            widget.text,
            style: GoogleFonts.roboto(
              fontSize: 18,
              color: _textColor,
            ),
          ).tr(),
        ),
      ),
    );
  }
}
