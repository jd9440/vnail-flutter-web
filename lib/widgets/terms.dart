import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';

class TermsConditionWidget extends StatelessWidget {
  final int selectedIndex;

  TermsConditionWidget(this.selectedIndex);

  AssetGenImage getImage() {
    switch (selectedIndex) {
      case 0:
        return Assets.images.deliveryTc;
      case 1:
        return Assets.images.deliveryRp;
      default:
        return Assets.images.deliveryDisclaimer;
    }
  }

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<HomePageProvider>();
    return Container(
      decoration: BoxDecoration(
        color: VNailColors.mainBgColor,
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      padding: EdgeInsets.fromLTRB(40, 82, 144, 164),
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 80),
            child: TextWidget.titleText(
              LocaleKeys.terms_conditions,
              textColor: VNailColors.colorStyle5,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextWidget.roboto400Normal(
                      LocaleKeys.category,
                      fontSize: 30,
                      fontWeight: FontWeight.w700,
                      textColor: VNailColors.colorStyle5,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextWidget.footerTextStyle(
                      LocaleKeys.delivery,
                      fontSize: 24,
                      fontWeight: (selectedIndex == 0)
                          ? FontWeight.w700
                          : FontWeight.w400,
                      onClickAction: () =>
                          _provider.updateHomeContent(TermsConditionWidget(0)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextWidget.footerTextStyle(
                      LocaleKeys.return_policy,
                      fontSize: 24,
                      fontWeight: (selectedIndex == 1)
                          ? FontWeight.w700
                          : FontWeight.w400,
                      onClickAction: () =>
                          _provider.updateHomeContent(TermsConditionWidget(1)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextWidget.footerTextStyle(
                      LocaleKeys.disclaimer,
                      fontSize: 24,
                      fontWeight: (selectedIndex == 2)
                          ? FontWeight.w700
                          : FontWeight.w400,
                      onClickAction: () =>
                          _provider.updateHomeContent(TermsConditionWidget(2)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Expanded(
                flex: 10,
                child: Image(
                  key: UniqueKey(),
                  image: getImage(),
                  fit: BoxFit.fill,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
