import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/src/provider.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/podo/userdata.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/DBUtils.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';

class ProfileWidget extends StatefulWidget {
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final TextEditingController _nameController = TextEditingController(),
      _emailController = TextEditingController(),
      _phoneController = TextEditingController(),
      _addressController = TextEditingController();

  int _selectCurrentWidget = 0;

  toggleWidget(int index) {
    _selectCurrentWidget = index;
    setState(() {});
  }

  Widget getCurrentWidget(int index, UserData userData) {
    switch (index) {
      case 0:
        _nameController.text = userData.userName;
        _phoneController.text = userData.userPhone;
        _emailController.text = userData.userEmail;
        _addressController.text = userData.userStreet +
            " " +
            userData.userStreet1 +
            " " +
            userData.userCity +
            " " +
            userData.userState +
            " " +
            userData.userZip +
            " " +
            userData.userCountry;
        return PersonalInformationWidget(
          nameController: _nameController,
          phoneController: _phoneController,
          emailController: _emailController,
          addressController: _addressController,
        );
      case 1:
        return ReceiptWidget();
      default:
        return SizedBox();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _phoneController.dispose();
    _addressController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _homeProvider = context.read<HomePageProvider>();
    return ValueListenableBuilder<Box<String>>(
      valueListenable: Hive.box<String>(DBUtils.userBox).listenable(),
      builder: (context, value, child) {
        String _stringData = value.get(
          DBUtils.userKey,
          defaultValue: userDataToJson(
            UserData(),
          ),
        )!;
        UserData _data = userDataFromJson(_stringData);
        return Container(
          decoration: BoxDecoration(
            color: VNailColors.mainBgColor,
            image: DecorationImage(
              image: Assets.images.designerBg,
              alignment: Alignment.center,
              fit: BoxFit.cover,
            ),
          ),
          padding: EdgeInsets.fromLTRB(40, 0, 40, 200),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 40,
              ),
              TextWidget.titleText(
                'Hello ${_data.userName}!!!',
                isUpperCase: true,
                textColor: VNailColors.colorStyle5,
              ),
              SizedBox(
                height: 40,
              ),
              TextWidget.productDescription(
                'Loyalty Points : ${_data.loyaltyPoints.toString()}',
                textColor: VNailColors.colorStyle4,
                fontSize: 36,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWidget.footerTextStyle(
                          LocaleKeys.personal_info,
                          fontSize: 24,
                          fontWeight: (_selectCurrentWidget == 0)
                              ? FontWeight.w700
                              : FontWeight.w400,
                          onClickAction: () => toggleWidget(0),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextWidget.footerTextStyle(
                          LocaleKeys.order_history,
                          fontSize: 24,
                          fontWeight: (_selectCurrentWidget == 1)
                              ? FontWeight.w700
                              : FontWeight.w400,
                          onClickAction: () => toggleWidget(1),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextWidget.footerTextStyle(
                          LocaleKeys.logout,
                          fontSize: 24,
                          fontWeight: (_selectCurrentWidget == 2)
                              ? FontWeight.w700
                              : FontWeight.w400,
                          onClickAction: () {
                            _homeProvider.clearDataAndLogOut(context);
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 64,
                  ),
                  Flexible(
                    flex: 7,
                    child: getCurrentWidget(_selectCurrentWidget, _data),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}

class PersonalInformationWidget extends StatelessWidget {
  const PersonalInformationWidget({
    Key? key,
    required TextEditingController nameController,
    required TextEditingController phoneController,
    required TextEditingController emailController,
    required TextEditingController addressController,
  })  : _nameController = nameController,
        _phoneController = phoneController,
        _emailController = emailController,
        _addressController = addressController,
        super(key: key);

  final TextEditingController _nameController;
  final TextEditingController _phoneController;
  final TextEditingController _emailController;
  final TextEditingController _addressController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FieldWidget(LocaleKeys.name_profile_hint, _nameController),
        SizedBox(
          height: 16,
        ),
        FieldWidget(LocaleKeys.mobile_profile_hint, _phoneController),
        SizedBox(
          height: 16,
        ),
        FieldWidget(LocaleKeys.email_profile_hint, _emailController),
        SizedBox(
          height: 16,
        ),
        FieldWidget(LocaleKeys.address_profile_hint, _addressController),
        SizedBox(
          height: 16,
        ),
      ],
    );
  }
}

class FieldWidget extends StatefulWidget {
  final String hintText;
  final TextEditingController textEditingController;

  FieldWidget(this.hintText, this.textEditingController);

  @override
  _FieldWidgetState createState() => _FieldWidgetState();
}

class _FieldWidgetState extends State<FieldWidget> {
  bool canEdit = true;

  void changeIcon() {
    canEdit = !canEdit;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(48, 8, 48, 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Color.fromRGBO(255, 2555, 255, 0.3),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextWidget.roboto400Normal(
            tr(widget.hintText),
            fontWeight: FontWeight.w500,
          ),
          Expanded(
            child: TextFormField(
              controller: widget.textEditingController,
              decoration: InputDecoration(
                isDense: true,
                border: InputBorder.none,
                errorBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                focusedErrorBorder: InputBorder.none,
                contentPadding: EdgeInsets.only(left: 16),
              ),
              cursorColor: VNailColors.colorStyle5,
              enabled: canEdit,
              maxLines: null,
            ),
          ),
          (canEdit)
              ? IconButton(
                  icon: Icon(
                    Icons.edit_rounded,
                    color: VNailColors.colorStyle5,
                    size: 16,
                  ),
                  onPressed: () => changeIcon())
              : IconButton(
                  icon: Icon(
                    Icons.edit_off,
                    color: VNailColors.colorStyle5,
                    size: 16,
                  ),
                  onPressed: () => changeIcon()),
        ],
      ),
    );
  }
}

class ReceiptWidget extends StatefulWidget {
  @override
  _ReceiptWidgetState createState() => _ReceiptWidgetState();
}

class _ReceiptWidgetState extends State<ReceiptWidget> {
  List<bool> _isExpanded = [false, false, false];

  toggleExpansion(int index) {
    _isExpanded[index] = !_isExpanded[index];
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Theme(
        data: Theme.of(context).copyWith(
          cardColor: Colors.transparent,
        ),
        child: ExpansionPanelList(
          elevation: 0,
          expansionCallback: (panelIndex, isExpanded) =>
              toggleExpansion(panelIndex),
          children: [
            ExpansionPanel(
              headerBuilder: (context, isExpanded) => Container(
                color: Colors.transparent,
                child: Row(
                  children: [
                    TextWidget.roboto400Normal(
                      tr(LocaleKeys.receipt_number) + "12345678",
                      fontWeight: FontWeight.w700,
                      fontSize: 18,
                    ),
                    TextWidget.roboto400Normal(
                      'Delivered on ',
                    ),
                  ],
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
              ),
              isExpanded: _isExpanded[0],
              canTapOnHeader: true,
              body: Container(
                height: MediaQueryUtils(context).height * 0.35,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Color.fromRGBO(255, 255, 255, 0.3),
                ),
                margin: EdgeInsets.only(top: 32, bottom: 32),
                padding: EdgeInsets.fromLTRB(32, 36, 32, 36),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    ProductTitleWidget(
                      tr(LocaleKeys.product),
                      tr(LocaleKeys.price),
                      tr(LocaleKeys.quantity),
                      tr(LocaleKeys.subtotal),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    ProductDescriptionWidget(
                      '有機利古里亞蜂蜜手部及身體沐浴乳',
                      '200\$',
                      '02',
                      '400\$',
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    ProductDescriptionWidget(
                      '有機利古里亞蜂蜜手部及身體沐浴乳',
                      '200\$',
                      '02',
                      '400\$',
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Divider(
                      color: VNailColors.colorStyle4,
                      height: 16,
                    ),
                    ProductTitleWidget(
                      tr(LocaleKeys.total),
                      "",
                      "04",
                      "810.00 \$",
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ProductDescriptionWidget extends StatelessWidget {
  final String str1, str2, str3, str4;

  ProductDescriptionWidget(this.str1, this.str2, this.str3, this.str4);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(flex: 6, child: TextWidget.roboto400Normal(str1)),
        Expanded(flex: 2, child: TextWidget.roboto400Normal(str2)),
        Expanded(flex: 2, child: TextWidget.roboto400Normal(str3)),
        Expanded(flex: 2, child: TextWidget.roboto400Normal(str4)),
      ],
    );
  }
}

class ProductTitleWidget extends StatelessWidget {
  final FontWeight fontWeight = FontWeight.w700;
  final String str1, str2, str3, str4;

  ProductTitleWidget(this.str1, this.str2, this.str3, this.str4);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          flex: 6,
          child: TextWidget.roboto400Normal(str1, fontWeight: fontWeight),
        ),
        Expanded(
            flex: 2,
            child: TextWidget.roboto400Normal(str2, fontWeight: fontWeight)),
        Expanded(
            flex: 2,
            child: TextWidget.roboto400Normal(str3, fontWeight: fontWeight)),
        Expanded(
            flex: 2,
            child: TextWidget.roboto400Normal(str4, fontWeight: fontWeight)),
      ],
    );
  }
}
