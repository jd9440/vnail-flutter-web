import 'package:flutter/material.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final IconData icon;

  final VoidCallback onClickAction;
  final double? width;

  CustomButton(this.text, this.icon, this.onClickAction, {this.width});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClickAction,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: VNailColors.colorStyle4,
        ),
        width: width ?? MediaQueryUtils(context).width * 0.2,
        padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
        margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextWidget.roboto400Normal(
              text,
              fontWeight: FontWeight.w500,
              fontSize: 18,
              textColor: Colors.white,
            ),
            SizedBox(
              width: 8,
            ),
            Icon(icon, size: 20, color: Colors.white),
          ],
        ),
      ),
    );
  }
}
