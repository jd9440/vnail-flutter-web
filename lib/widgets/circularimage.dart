import 'package:flutter/material.dart';
import 'package:vnail/gen/assets.gen.dart';

class CircularImage extends StatelessWidget {
  final double circularRadius;
  final AssetGenImage image;
  final BoxFit? boxFit;

  CircularImage(
    this.circularRadius,
    this.image, {
    this.boxFit,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(circularRadius),
      child: Image(
        image: image,
        fit: boxFit ?? BoxFit.cover,
      ),
    );
  }
}
