import 'package:flutter/material.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';

class PageImage extends StatelessWidget {
  final String title;
  final AssetGenImage image;
  PageImage(this.title, this.image);
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        ColorFiltered(
          colorFilter: ColorFilter.mode(
            Colors.black12,
            BlendMode.color,
          ),
          child: Image(
            image: image,
            fit: BoxFit.fill,
            width: MediaQueryUtils(context).width,
            height: MediaQueryUtils(context).height *
                0.4 *
                MediaQueryUtils(context).aspectRatio,
          ),
        ),
        TextWidget.titleText(
          title,
        ),
      ],
    );
  }
}
