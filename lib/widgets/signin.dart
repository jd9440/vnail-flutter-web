import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/utility/validation.dart';
import 'package:vnail/widgets/textinput.dart';

class SignInWidget extends StatefulWidget {
  @override
  _SignInWidgetState createState() => _SignInWidgetState();
}

class _SignInWidgetState extends State<SignInWidget> {
  late final TextEditingController _mobileController, _passwordController;
  late final FocusNode _mobileNode, _passwordNode;

  @override
  void initState() {
    _mobileController = TextEditingController();
    _passwordController = TextEditingController();
    _mobileNode = FocusNode();
    _passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _mobileController.dispose();
    _passwordController.dispose();
    _mobileNode.dispose();
    _passwordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<HomePageProvider>();
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
                child: TextWidget.serviceNameText(LocaleKeys.signin),
              ),
              IconButton(
                icon: Icon(
                  Icons.close,
                  color: VNailColors.colorStyle4,
                  size: 16,
                ),
                onPressed: () => _provider.popDialog(context),
              ),
            ],
          ),
          SizedBox(
            height: MediaQueryUtils(context).height * 0.1,
          ),
          TextInputWidget(
            TextInputType.text,
            controller: _mobileController,
            focusNode: _mobileNode,
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            maxLength: 10,
            maxLines: 1,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.email),
            onFieldSubmittedAction: (data) => ValidateUtils.isValidMobile(data),
          ),
          TextInputWidget(
            TextInputType.text,
            focusNode: _passwordNode,
            controller: _passwordController,
            maxLength: 24,
            maxLines: 1,
            onFieldSubmittedAction: (data) =>
                ValidateUtils.isRequiredCheck(data),
            circularRadius: 20,
            contentPaddingTop: 8,
            contentPaddingBottom: 8,
            textAlignVertical: TextAlignVertical.center,
            textAlignment: TextAlign.start,
            hintText: tr(LocaleKeys.password_hint),
            errorText: tr(LocaleKeys.required_field),
            obscureText: _provider.obscureText,
            suffixIcon: (_provider.obscureText)
                ? IconButton(
                    iconSize: 18,
                    icon: Icon(
                      Icons.visibility_off,
                      color: VNailColors.colorStyle4,
                    ),
                    onPressed: () => _provider.toggleObscureValue(),
                  )
                : IconButton(
                    iconSize: 18,
                    icon: Icon(
                      Icons.visibility,
                      color: VNailColors.colorStyle4,
                    ),
                    onPressed: () => _provider.toggleObscureValue(),
                  ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, bottom: 16),
            child: TextWidget.serviceDescriptionText(
              LocaleKeys.forgot_password_question,
              fontWeight: FontWeight.w700,
              onClickAction: () => _provider.changeIndex(2),
            ),
          ),
          TextButton(
            onPressed: () => _provider.logAndGetUserData(
              context,
              _mobileController.text,
              _passwordController.text,
            ),
            child: Container(
              width: MediaQueryUtils(context).width * 0.2,
              height: MediaQueryUtils(context).width * 0.035,
              margin: EdgeInsets.only(left: 16, right: 16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: VNailColors.colorStyle5,
              ),
              child: TextWidget.serviceDescriptionText(
                LocaleKeys.login,
                fontSize: 18,
                fontWeight: FontWeight.w500,
                textColor: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextWidget.serviceDescriptionText(
                  LocaleKeys.account_registration_question,
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
                TextWidget.serviceDescriptionText(
                  LocaleKeys.sign_up,
                  onClickAction: () => _provider.changeIndex(1),
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                  textDecoration: TextDecoration.underline,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
