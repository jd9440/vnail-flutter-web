import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/brand_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';

import 'pageimage.dart';

class Brands extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => BrandProvider(),
      child: Container(
        decoration: BoxDecoration(
          color: VNailColors.mainBgColor,
          image: DecorationImage(
            image: Assets.images.designerBg,
            alignment: Alignment.center,
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            PageImage(
              LocaleKeys.our_brand,
              Assets.images.ourBrand,
            ),
            BrandSelectionPanel(),
            BrandBodyWidget(),
          ],
        ),
      ),
    );
  }
}

class BrandBodyWidget extends StatelessWidget {
  const BrandBodyWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<BrandProvider>();
    return Padding(
      padding: const EdgeInsets.fromLTRB(32, 64, 32, 128),
      child: Image(
        repeat: ImageRepeat.noRepeat,
        key: UniqueKey(),
        excludeFromSemantics: true,
        image: AssetImage(_provider.getImagePath(context)),
      ),
    );
  }
}

class BrandSelectionPanel extends StatelessWidget {
  const BrandSelectionPanel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _provider = context.read<BrandProvider>();
    return Container(
      padding: EdgeInsets.all(32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          TextWidget.footerTextStyle(
            LocaleKeys.adorelle,
            fontSize: 24,
            fontWeight: FontWeight.w700,
            onClickAction: () => _provider.changeBrandSelection(0),
          ),
          SizedBox(
            width: 24,
          ),
          TextWidget.footerTextStyle(
            LocaleKeys.body_drench,
            fontSize: 24,
            fontWeight: FontWeight.w700,
            onClickAction: () => _provider.changeBrandSelection(1),
          ),
          SizedBox(
            width: 24,
          ),
          TextWidget.footerTextStyle(
            LocaleKeys.cuccio,
            fontSize: 24,
            fontWeight: FontWeight.w700,
            onClickAction: () => _provider.changeBrandSelection(2),
          ),
          SizedBox(
            width: 24,
          ),
          TextWidget.footerTextStyle(
            LocaleKeys.footlogix,
            fontSize: 24,
            fontWeight: FontWeight.w700,
            onClickAction: () => _provider.changeBrandSelection(3),
          ),
          SizedBox(
            width: 24,
          ),
          TextWidget.footerTextStyle(
            LocaleKeys.main_beach,
            fontSize: 24,
            fontWeight: FontWeight.w700,
            onClickAction: () => _provider.changeBrandSelection(4),
          ),
        ],
      ),
    );
  }
}
