import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/cart_provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/providers/product_provider.dart';
import 'package:vnail/providers/wishlist_provider.dart';
import 'package:vnail/response/product_response.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/products.dart';
import 'package:vnail/widgets/welcome_cart.dart';

import 'custom_widget.dart';

class ProductDescriptionContent extends StatelessWidget {
  final Result individualProduct;

  ProductDescriptionContent(
    this.individualProduct,
  );

  String getCategoryText() {
    if (individualProduct.productCategary == 'Others') {
      return tr(LocaleKeys.others);
    } else if (individualProduct.productCategary == 'Foot') {
      return tr(LocaleKeys.foot);
    } else if (individualProduct.productCategary == 'Body Care') {
      return tr(LocaleKeys.body_care);
    } else if (individualProduct.productCategary == 'Hand & Nail') {
      return tr(LocaleKeys.hand_nail);
    } else {
      return tr(LocaleKeys.all_products);
    }
  }

  @override
  Widget build(BuildContext context) {
    final _provider = Provider.of<HomePageProvider>(context);
    return WillPopScope(
      onWillPop: () async {
        _provider.updateHomeContent(ProductContent(0));
        return await Future.value(true);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          image: DecorationImage(
            image: Assets.images.productsBg,
            alignment: Alignment.center,
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.only(top: 40, bottom: 160, left: 64, right: 40),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text.rich(
              TextSpan(
                text: tr(LocaleKeys.product_home_path),
                style: GoogleFonts.roboto(
                  fontSize: 24,
                  fontWeight: FontWeight.w400,
                  color: VNailColors.colorStyle4,
                ),
                children: <InlineSpan>[
                  TextSpan(
                    text: '${individualProduct.productCategary} > ',
                    style: GoogleFonts.roboto(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      color: VNailColors.colorStyle4,
                    ),
                  ),
                  TextSpan(
                    text: individualProduct.name,
                    style: GoogleFonts.roboto(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      color: VNailColors.colorStyle5,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 32,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  flex: 3,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, 32, 48, 32),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Image.asset(
                      Assets.images.bodyCareProduct.path,
                      fit: BoxFit.cover,
                      height: 350,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 18),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWidget.productDescription(
                          individualProduct.name,
                          fontSize: 36,
                          fontWeight: FontWeight.w500,
                          maxLines: 3,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextWidget.productDescription(
                          '${tr(LocaleKeys.price)}: ${individualProduct.price} \$',
                          fontSize: 30,
                          fontWeight: FontWeight.w400,
                          maxLines: 1,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        ProductActions(individualProduct: individualProduct),
                        SizedBox(
                          height: 40,
                        ),
                        Text.rich(
                          TextSpan(
                            text: tr(LocaleKeys.category),
                            style: GoogleFonts.roboto(
                              fontSize: 24,
                              fontWeight: FontWeight.w400,
                              color: VNailColors.colorStyle4,
                            ),
                            children: <InlineSpan>[
                              TextSpan(
                                text: getCategoryText(),
                                style: GoogleFonts.roboto(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700,
                                  color: VNailColors.colorStyle5,
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  flex: 5,
                ),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            TextWidget.roboto400Normal(
              LocaleKeys.product_desc,
              fontSize: 30,
              fontWeight: FontWeight.w700,
            ),
            SizedBox(
              height: 24,
            ),
            ListView(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                TextWidget.productDescription(
                  individualProduct.description,
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  maxLines: 100,
                ),
              ],
            ),
            SizedBox(
              height: 80,
            ),
            TextWidget.roboto400Normal(
              LocaleKeys.recently_viewed_products,
              fontSize: 30,
              fontWeight: FontWeight.w700,
            ),
            SizedBox(
              height: 80,
            ),
            RecentlyViewedList(),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}

class ProductActions extends StatelessWidget {
  final Result individualProduct;

  const ProductActions({
    Key? key,
    required this.individualProduct,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _homeProvider = context.read<HomePageProvider>();
    final _wishlistProvider = context.watch<WishListProvider>();
    final _cartProvider = context.watch<CartProvider>();

    bool isItemAlreadyAdded =
        _wishlistProvider.checkIfAlreadyExists(individualProduct);

    bool isItemAlreadyAddedInCart =
        _cartProvider.checkIfAlreadyExists(individualProduct);

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          flex: 1,
          child: CustomButton(
            LocaleKeys.add_to_cart,
            (isItemAlreadyAddedInCart)
                ? Icons.shopping_cart_rounded
                : Icons.shopping_cart_outlined,
            () => (isItemAlreadyAddedInCart)
                ? _cartProvider.removeProductFromCart(individualProduct)
                : _cartProvider.addProductToCart(individualProduct),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        Flexible(
          flex: 1,
          child: CustomButton(
            LocaleKeys.wishlist,
            (isItemAlreadyAdded) ? Icons.favorite : Icons.favorite_border,
            () => (isItemAlreadyAdded)
                ? _wishlistProvider.removeProductFromWishList(individualProduct)
                : _wishlistProvider.addProductToWishList(individualProduct),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        Flexible(
          flex: 1,
          child: CustomButton(
            LocaleKeys.checkout,
            Icons.shopping_bag_outlined,
            () {
              _cartProvider.setCartItems(individualProduct);
              _homeProvider.updateHomeContent(
                WelcomeCart(
                  PaymentFirstPage(),
                ),
              );
            },
          ),
        ),
        SizedBox(
          width: 16,
        ),
      ],
    );
  }
}

class RecentlyViewedList extends StatelessWidget {
  const RecentlyViewedList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _productsProvider = context.read<ProductProvider>();
    final _homeProvider = context.read<HomePageProvider>();
    final _recentList = _productsProvider.getRecentlyViewedProducts;
    return SizedBox(
      height: 300,
      child: ListView.builder(
        padding: EdgeInsets.only(
          left: 16,
          right: 16,
          top: 16,
          bottom: 16,
        ),
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        reverse: true,
        itemCount: _recentList.length,
        itemBuilder: (context, index) {
          final _currentItem = _recentList[index];
          return InkWell(
            onTap: () => _homeProvider.updateHomeContent(
              ProductDescriptionContent(_currentItem),
            ),
            child: RecentlyViewedItem(
              _currentItem.name,
            ),
          );
        },
      ),
    );
  }
}

class RecentlyViewedItem extends StatelessWidget {
  final String productName;

  RecentlyViewedItem(this.productName);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8),
      child: Column(
        children: [
          Flexible(
            flex: 8,
            child: Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Image(
                image: Assets.images.bodyCareProduct,
                fit: BoxFit.contain,
                height: 175,
                width: 150,
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          SizedBox(
            width: 200,
            child: TextWidget.productDescription(
              productName,
              maxLines: 2,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}

class CircularButton extends StatelessWidget {
  final VoidCallback onClickAction;
  final IconData iconData;

  CircularButton(this.iconData, this.onClickAction);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onClickAction,
      elevation: 2.0,
      fillColor: VNailColors.colorStyle3,
      child: Icon(
        iconData,
        color: Colors.white,
        size: 16,
      ),
      shape: CircleBorder(),
    );
  }
}

class ItemCount extends StatelessWidget {
  final int count;

  ItemCount(this.count);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      width: 46,
      decoration: BoxDecoration(
        color: VNailColors.colorStyle4,
        borderRadius: BorderRadius.circular(8),
      ),
      alignment: Alignment.center,
      child: TextWidget.productDescription(
        count.toString(),
        fontSize: 18,
        fontWeight: FontWeight.w500,
        textColor: Colors.white,
      ),
    );
  }
}
