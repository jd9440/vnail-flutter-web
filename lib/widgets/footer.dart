import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/src/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/utility/url_services.dart';
import 'package:vnail/widgets/terms.dart';

class FooterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _provider = context.read<HomePageProvider>();
    return Container(
      color: VNailColors.footerColor,
      alignment: Alignment.topCenter,
      padding: EdgeInsets.only(
        top: 64,
        bottom: 64,
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        rowMainAxisSize: MainAxisSize.max,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ResponsiveRowColumnItem(
              child: Flexible(
            child: Align(
              alignment: Alignment.centerLeft,
              child: Image(
                image: Assets.images.logo,
                height: 150,
                width: 150,
              ),
            ),
            flex: 2,
          )),
          ResponsiveRowColumnItem(
            child: Flexible(
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                rowMainAxisSize: MainAxisSize.max,
                rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
                rowCrossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ResponsiveRowColumnItem(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: 24),
                            child: TextWidget.serviceDescriptionText(
                              LocaleKeys.terms_conditions,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: TextWidget.footerTextStyle(
                              LocaleKeys.delivery,
                              onClickAction: () => _provider
                                  .updateHomeContent(TermsConditionWidget(0)),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: TextWidget.footerTextStyle(
                              LocaleKeys.return_policy,
                              onClickAction: () => _provider
                                  .updateHomeContent(TermsConditionWidget(1)),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 16),
                            child: TextWidget.footerTextStyle(
                              LocaleKeys.disclaimer,
                              onClickAction: () => _provider
                                  .updateHomeContent(TermsConditionWidget(2)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  ResponsiveRowColumnItem(
                      child: Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: TextWidget.serviceDescriptionText(
                            LocaleKeys.contact_us,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 16),
                          child: TextWidget.footerTextWithIconStyle(
                            LocaleKeys.vnail_whatsapp,
                            FontAwesomeIcons.whatsapp,
                            onClickAction: () =>
                                URLServiceUtils(context).openWhatsApp(),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 16),
                          child: TextWidget.footerTextWithIconStyle(
                            LocaleKeys.vnail_phone,
                            Icons.phone_outlined,
                            onClickAction: () =>
                                URLServiceUtils(context).openPhoneNumber(),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 16),
                          child: TextWidget.footerTextWithIconStyle(
                            LocaleKeys.vnail_email,
                            Icons.email_outlined,
                            onClickAction: () =>
                                URLServiceUtils(context).openEmail(),
                          ),
                        ),
                      ],
                    ),
                  )),
                  ResponsiveRowColumnItem(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(bottom: 20),
                            child: TextWidget.serviceDescriptionText(
                              LocaleKeys.follow_us,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              TextWidget.footerIconStyle(
                                FontAwesomeIcons.instagram,
                                onClickAction: () => URLServiceUtils(context)
                                    .openInstagramPage(),
                              ),
                              TextWidget.footerIconStyle(
                                FontAwesomeIcons.facebookSquare,
                                onClickAction: () =>
                                    URLServiceUtils(context).openFacebookPage(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  ResponsiveRowColumnItem(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child: TextWidget.serviceDescriptionText(
                            LocaleKeys.shop_details,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 16),
                          child: TextWidget.footerTextStyle(
                            LocaleKeys.shop_address_en,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 16),
                          child: TextWidget.footerTextStyle(
                            LocaleKeys.shop_address_ch,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 16),
                          child: TextWidget.footerTextStyle(
                            LocaleKeys.shop_time,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              flex: 8,
            ),
          ),
        ],
      ),
    );
  }
}
