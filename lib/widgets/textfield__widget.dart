import 'package:flutter/material.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';

import 'member_offer.dart';

class TextFieldWidget extends StatelessWidget {
  final String text;
  final TextEditingController textEditingController;
  final FocusNode focusNode;

  TextFieldWidget(
    this.text,
    this.focusNode,
    this.textEditingController,
  );

  Widget build(BuildContext context) {
    return SizedBox(
      height: 64,
      width: MediaQueryUtils(context).width * 0.7,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: MediaQueryUtils(context).width * 0.3,
            child: TextWidget.roboto400Normal(
              text,
              textColor: VNailColors.colorStyle4,
            ),
          ),
          SizedBox(
            height: 64,
            width: MediaQueryUtils(context).width * 0.4,
            child: UnderlineTextField(
              textEditingController: textEditingController,
              focusNode: focusNode,
            ),
          ),
        ],
      ),
    );
  }
}