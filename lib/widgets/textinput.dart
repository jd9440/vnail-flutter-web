import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vnail/utility/color.dart';

import '../utility/mediaquery.dart';

class TextInputWidget extends StatelessWidget {
  final String hintText, errorText;
  final double circularRadius,
      leftMargin,
      rightMargin,
      topMargin,
      bottomMargin,
      fontSize;
  final Widget? suffixWidget;
  final Color textColor = VNailColors.colorStyle4,
      fillColor = VNailColors.mainBgColor;
  final TextInputType inputType;
  final TextEditingController controller;
  final String? Function(String?)? validator;
  final void Function(String)? onFieldSubmittedAction;
  final FocusNode focusNode;
  final bool autoFocus, obscureText, isEnabled, showCounter;
  final TextInputAction textInputAction;
  final double contentPaddingTop, contentPaddingBottom;
  final TextAlignVertical textAlignVertical;
  final int maxLines, maxLength;
  final TextAlign textAlignment;
  final Widget suffixIcon;

  TextInputWidget(
    this.inputType, {
    this.circularRadius = 100,
    this.textAlignVertical = TextAlignVertical.center,
    required this.controller,
    this.contentPaddingTop = 4,
    this.contentPaddingBottom = 4,
    this.textInputAction = TextInputAction.next,
    this.onFieldSubmittedAction,
    this.autoFocus = false,
    required this.focusNode,
    this.suffixIcon = const SizedBox(),
    this.validator,
    this.hintText = "",
    this.maxLines = 1,
    this.maxLength = 35,
    this.errorText = "",
    this.textAlignment = TextAlign.start,
    this.obscureText = false,
    this.showCounter = false,
    this.isEnabled = true,
    this.fontSize = 16,
    this.leftMargin = 16,
    this.rightMargin = 16,
    this.topMargin = 8,
    this.bottomMargin = 8,
    this.suffixWidget,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: VNailColors.colorStyle4,
      enabled: isEnabled,
      onFieldSubmitted: onFieldSubmittedAction,
      textInputAction: textInputAction,
      style: TextStyle(
        color: textColor,
      ),
      autofocus: autoFocus,
      focusNode: focusNode,
      validator: validator,
      expands: false,
      obscureText: obscureText,
      maxLines: maxLines,
      controller: controller,
      textAlign: textAlignment,
      textAlignVertical: textAlignVertical,
      keyboardType: inputType,
      autocorrect: true,
      maxLength: maxLength,
      decoration: InputDecoration(
        suffix: suffixWidget,
        suffixIcon: suffixIcon,
        counterText:
            (showCounter) ? '${controller.text.length} / $maxLength' : "",
        errorStyle: TextStyle(
          height: 0.8,
          color: Colors.red,
          fontSize: 12,
        ),
        helperText: "",
        hintStyle: GoogleFonts.roboto(
          color: VNailColors.colorStyle4,
          fontSize: fontSize * MediaQueryUtils(context).scaleFactor,
          fontWeight: FontWeight.w500,
        ),
        contentPadding: EdgeInsets.fromLTRB(24, 0, 0, 0),
        isDense: true,
        filled: true,
        fillColor: fillColor,
        hintText: hintText,
        errorText: errorText,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: VNailColors.mainBgColor),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: VNailColors.mainBgColor, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: VNailColors.mainBgColor, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: VNailColors.mainBgColor, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}
