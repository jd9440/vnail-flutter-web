import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/widgets/forgotpassword.dart';
import 'package:vnail/widgets/signin.dart';
import 'package:vnail/widgets/signup.dart';

class AlertDialogBodyWidget extends StatefulWidget {
  @override
  _AlertDialogBodyWidgetState createState() => _AlertDialogBodyWidgetState();
}

class _AlertDialogBodyWidgetState extends State<AlertDialogBodyWidget> {
  @override
  Widget build(BuildContext context) {
    final _homePageProvider = context.watch<HomePageProvider>();
    return IndexedStack(
      alignment: Alignment.topCenter,
      children: [
        SignInWidget(),
        SignUpWidget(),
        ForgotPasswordWidget(),
      ],
      sizing: StackFit.passthrough,
      index: _homePageProvider.currentAlertDialogWidgetIndex,
    );
  }
}
