import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/circularimage.dart';
import 'package:vnail/widgets/pageimage.dart';

class AboutUsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: VNailColors.mainBgColor,
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: [
          ResponsiveRowColumnItem(
              child: PageImage(
            LocaleKeys.about_header_txt,
            Assets.images.aboutusbg,
          )),
          ResponsiveRowColumnItem(
              child: Container(
            width: MediaQueryUtils(context).width,
            padding: const EdgeInsets.fromLTRB(32, 32, 32, 32),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    child: Image(
                      image: Assets.images.vnailBeauty,
                      fit: BoxFit.fill,
                    ),
                  ),
                  flex: 4,
                ),
                Expanded(
                  flex: 6,
                  child: Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 24, bottom: 24),
                          child: TextWidget.titleText(
                            LocaleKeys.vnail_beauty,
                            textColor: VNailColors.serviceTextColor,
                          ),
                        ),
                        TextWidget.roboto400Normal(LocaleKeys.about1,
                            textColor: VNailColors.colorStyle4, fontSize: 24),
                        SizedBox(
                          height: 20,
                        ),
                        TextWidget.roboto400Normal(LocaleKeys.about2,
                            textColor: VNailColors.colorStyle4, fontSize: 24),
                        SizedBox(
                          height: 20,
                        ),
                        TextWidget.roboto400Normal(LocaleKeys.about3,
                            textColor: VNailColors.colorStyle4, fontSize: 24),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )),
          ResponsiveRowColumnItem(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(42, 0, 42, 0),
            child: TextWidget.roboto400Normal(LocaleKeys.about4,
                textColor: VNailColors.colorStyle4, fontSize: 24),
          )),
          ResponsiveRowColumnItem(
              child: SizedBox(
            height: 20,
          )),
          ResponsiveRowColumnItem(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(42, 0, 42, 0),
            child: TextWidget.roboto400Normal(LocaleKeys.about5,
                textColor: VNailColors.colorStyle4, fontSize: 24),
          )),
          ResponsiveRowColumnItem(
              child: SizedBox(
            height: 32,
          )),
          ResponsiveRowColumnItem(
              child: Padding(
            padding: EdgeInsets.fromLTRB(56, 32, 56, 32),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 1,
                  child: CircularImage(
                    32,
                    Assets.images.about1,
                    boxFit: BoxFit.none,
                  ),
                ),
                SizedBox(
                  width: 82,
                ),
                Expanded(
                  flex: 1,
                  child: CircularImage(
                    32,
                    Assets.images.about2,
                    boxFit: BoxFit.none,
                  ),
                ),
                SizedBox(
                  width: 82,
                ),
                Expanded(
                  flex: 1,
                  child: CircularImage(
                    32,
                    Assets.images.about3,
                    boxFit: BoxFit.none,
                  ),
                ),
              ],
            ),
          )),
          ResponsiveRowColumnItem(
              child: SizedBox(
            height: 64,
          )),
        ],
      ),
    );
  }
}
