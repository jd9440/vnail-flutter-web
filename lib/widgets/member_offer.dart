import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/circularimage.dart';
import 'package:vnail/widgets/pageimage.dart';

import 'textfield__widget.dart';

class MemberOffer extends StatefulWidget {
  @override
  _MemberOfferState createState() => _MemberOfferState();
}

class _MemberOfferState extends State<MemberOffer> {
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneNumberController = TextEditingController();
  final _pwdController = TextEditingController();
  final _confirmPwdController = TextEditingController();

  final _nameFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _phoneNumberFocusNode = FocusNode();
  final _pwdFocusNode = FocusNode();
  final _confirmPwdFocusNode = FocusNode();

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNumberController.dispose();
    _pwdController.dispose();
    _confirmPwdController.dispose();

    _nameFocusNode.dispose();
    _emailFocusNode.dispose();
    _phoneNumberFocusNode.dispose();
    _pwdFocusNode.dispose();
    _confirmPwdFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: VNailColors.mainBgColor,
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      child: Flex(
        direction: Axis.vertical,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          PageImage(
            LocaleKeys.member_offer,
            Assets.images.memberOffer,
          ),
          SizedBox(
            height: 36,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(42, 42, 42, 56),
            child: CircularImage(32, Assets.images.memberBox),
          ),
          Container(
            child: Column(
              children: [
                TextFieldWidget(
                    LocaleKeys.name_hint, _nameFocusNode, _nameController),
                TextFieldWidget(LocaleKeys.mobile_number_hint,
                    _phoneNumberFocusNode, _phoneNumberController),
                TextFieldWidget(
                    LocaleKeys.email, _emailFocusNode, _emailController),
                TextFieldWidget(
                    LocaleKeys.password_hint, _pwdFocusNode, _pwdController),
                TextFieldWidget(LocaleKeys.confirmpassword_hint,
                    _confirmPwdFocusNode, _confirmPwdController),
              ],
            ),
          ),
          SizedBox(
            height: 32,
          ),
          Center(
            child: ElevatedButton(
              onPressed: () {},
              child: TextWidget.serviceButtonText(LocaleKeys.sign_up),
              style: ElevatedButton.styleFrom(
                primary: VNailColors.colorStyle3,
                shape: StadiumBorder(),
                minimumSize: Size(140, 48),
              ),
            ),
          ),
          SizedBox(
            height: 128,
          ),
        ],
      ),
    );
  }
}

class UnderlineTextField extends StatelessWidget {
  const UnderlineTextField({
    Key? key,
    required this.textEditingController,
    required this.focusNode,
  }) : super(key: key);

  final TextEditingController textEditingController;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textEditingController,
      focusNode: focusNode,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return tr(LocaleKeys.required_field);
        }
        return null;
      },
      decoration: InputDecoration(
        fillColor: VNailColors.colorStyle4,
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: VNailColors.colorStyle4,
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: VNailColors.colorStyle4,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: VNailColors.colorStyle4,
          ),
        ),
        disabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: VNailColors.colorStyle4,
          ),
        ),
        focusedErrorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
          ),
        ),
        errorBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
          ),
        ),
        errorText: tr(LocaleKeys.required_field),
        contentPadding: EdgeInsets.only(left: 16, right: 16),
      ),
      cursorColor: VNailColors.colorStyle4,
    );
  }
}
