import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/appointment.dart';
import 'package:vnail/widgets/products.dart';
import 'package:vnail/widgets/services.dart';

class HomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ResponsiveRowColumn(
      columnMainAxisAlignment: MainAxisAlignment.start,
      columnMainAxisSize: MainAxisSize.min,
      layout: ResponsiveRowColumnType.COLUMN,
      children: [
        ResponsiveRowColumnItem(child: VNailWelcomeScreen()),
        ResponsiveRowColumnItem(child: ServiceWidget()),
        ResponsiveRowColumnItem(child: ProductsWidget()),
        ResponsiveRowColumnItem(child: AppointmentBookingWidget()),
      ],
    );
  }
}

class VNailWelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      children: [
        ColorFiltered(
          colorFilter: ColorFilter.mode(
            Colors.black12,
            BlendMode.color,
          ),
          child: Image(
            image: Assets.images.vnailBg,
            width: MediaQueryUtils(context).width,
            fit: BoxFit.cover,
            repeat: ImageRepeat.repeat,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 72),
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            columnMainAxisSize: MainAxisSize.max,
            columnCrossAxisAlignment: CrossAxisAlignment.start,
            columnMainAxisAlignment: MainAxisAlignment.center,
            children: [
              ResponsiveRowColumnItem(
                  child: TextWidget.titleText(LocaleKeys.welcome_title_text)),
              ResponsiveRowColumnItem(
                  child: TextWidget.subTitleText(
                      LocaleKeys.welcome_subtitle_text)),
            ],
          ),
        ),
      ],
    );
  }
}

class ServiceWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<HomePageProvider>();
    return Container(
      padding: EdgeInsets.only(left: 48, right: 48, top: 100, bottom: 100),
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.ROW,
        rowMainAxisSize: MainAxisSize.max,
        rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ResponsiveRowColumnItem(
            child: Flexible(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 150),
                child:
                    TextWidget.serviceTitleText(LocaleKeys.service_title_text),
              ),
              flex: 4,
            ),
          ),
          ResponsiveRowColumnItem(
            child: Flexible(
              flex: 6,
              child: ResponsiveRowColumn(
                layout: ResponsiveRowColumnType.ROW,
                rowMainAxisAlignment: MainAxisAlignment.spaceBetween,
                rowMainAxisSize: MainAxisSize.max,
                rowCrossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ResponsiveRowColumnItem(
                      child: ServiceItemWidget(
                    Assets.images.manicureService.path,
                    LocaleKeys.manicure,
                    onClickAction: () => _provider.updateHomeContent(
                      ServiceContent(0),
                    ),
                  )),
                  ResponsiveRowColumnItem(
                      child: ServiceItemWidget(
                    Assets.images.pedicureService.path,
                    LocaleKeys.pedicure,
                    onClickAction: () => _provider.updateHomeContent(
                      ServiceContent(1),
                    ),
                  )),
                  ResponsiveRowColumnItem(
                      child: ServiceItemWidget(
                    Assets.images.treatment.path,
                    LocaleKeys.treatment,
                    onClickAction: () => _provider.updateHomeContent(
                      ServiceContent(2),
                    ),
                  )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ServiceItemWidget extends StatelessWidget {
  late final String imagePath, serviceName;
  late final VoidCallback? onClickAction;

  ServiceItemWidget(this.imagePath, this.serviceName, {this.onClickAction});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(34),
          child: Image.asset(
            imagePath,
            height: MediaQueryUtils(context).width * 0.15,
            width: MediaQueryUtils(context).width * 0.12,
            isAntiAlias: true,
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 16),
          child: TextWidget.serviceNameText(
            serviceName,
          ),
        ),
        ElevatedButton(
          onPressed: onClickAction,
          child: TextWidget.serviceButtonText(LocaleKeys.booknow),
          style: ElevatedButton.styleFrom(
            primary: VNailColors.colorStyle3,
            shape: StadiumBorder(),
            minimumSize: Size(140, 48),
          ),
        )
      ],
    );
  }
}

class ProductsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _homePageProvider = context.read<HomePageProvider>();
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.only(
        top: MediaQueryUtils(context).height * 0.05,
        bottom: MediaQueryUtils(context).height * 0.1,
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Assets.images.productBgImage,
          fit: BoxFit.cover,
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.max,
        columnCrossAxisAlignment: CrossAxisAlignment.center,
        columnMainAxisAlignment: MainAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
              child: Padding(
            padding: const EdgeInsets.only(top: 80, bottom: 80),
            child: TextWidget.serviceTitleText(LocaleKeys.our_products,
                textColor: Colors.white),
          )),
          ResponsiveRowColumnItem(
              child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.ROW,
            rowMainAxisAlignment: MainAxisAlignment.center,
            children: [
              ResponsiveRowColumnItem(
                  child: ProductItemWidgetHome(
                Assets.images.bodyCareProduct.path,
                LocaleKeys.body_care,
                onClickAction: () =>
                    _homePageProvider.updateHomeContent(ProductContent(0)),
              )),
              ResponsiveRowColumnItem(
                  child: ProductItemWidgetHome(
                Assets.images.footProduct.path,
                LocaleKeys.foot,
                onClickAction: () =>
                    _homePageProvider.updateHomeContent(ProductContent(1)),
              )),
              ResponsiveRowColumnItem(
                  child: ProductItemWidgetHome(
                Assets.images.handNailProduct.path,
                LocaleKeys.hand_nail,
                onClickAction: () =>
                    _homePageProvider.updateHomeContent(ProductContent(2)),
              )),
              ResponsiveRowColumnItem(
                  child: ProductItemWidgetHome(
                Assets.images.otherProduct.path,
                LocaleKeys.others,
                onClickAction: () =>
                    _homePageProvider.updateHomeContent(ProductContent(3)),
              )),
            ],
          )),
        ],
      ),
    );
  }
}

class ProductItemWidgetHome extends StatelessWidget {
  late final String imagePath, serviceName;
  late final VoidCallback? onClickAction;

  ProductItemWidgetHome(this.imagePath, this.serviceName, {this.onClickAction});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClickAction,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 12, right: 12),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(34),
              child: Image.asset(
                imagePath,
                isAntiAlias: true,
                fit: BoxFit.fill,
                height: 247,
                width: 210,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 16),
            child: TextWidget.serviceNameText(
              serviceName,
              fontColor: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
