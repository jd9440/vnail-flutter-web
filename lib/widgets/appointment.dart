import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/utility/url_services.dart';

class AppointmentBookingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: VNailColors.mainBgColor,
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
          scale: 1,
        ),
      ),
      padding: EdgeInsets.only(top: 80, bottom: 160),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: [
          ResponsiveRowColumnItem(
              child: Padding(
            padding: const EdgeInsets.only(bottom: 64),
            child: TextWidget.serviceTitleText(LocaleKeys.book_appointment_msg),
          )),
          ResponsiveRowColumnItem(
              child: ElevatedButton.icon(
            icon: Image.asset(
              Assets.images.whatsappIcon.path,
              color: Colors.white,
            ),
            label: TextWidget.serviceButtonText(
              LocaleKeys.booknow,
            ),
            onPressed: () => URLServiceUtils(context).openWhatsApp(),
            style: ElevatedButton.styleFrom(
              primary: VNailColors.bookNowButtonColor,
              shape: StadiumBorder(),
              minimumSize: Size(200, 48),
            ),
          )),
        ],
      ),
    );
  }
}
