import 'package:auto_size_text_pk/auto_size_text_pk.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/src/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/cart_provider.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/response/product_response.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/device_utils.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/widgets/custom_widget.dart';

import 'custom_back_widget.dart';
import 'product_description.dart';
import 'textfield__widget.dart';

class WelcomeCart extends StatefulWidget {
  final Widget widget;

  WelcomeCart(this.widget);

  @override
  _WelcomeCartState createState() => _WelcomeCartState();
}

class _WelcomeCartState extends State<WelcomeCart> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 36, top: 40, right: 36, bottom: 40),
      child: widget.widget,
    );
  }
}

class PaymentFirstPage extends StatelessWidget {
  const PaymentFirstPage({
    Key? key,
  }) : super(key: key);

  Widget _buildCheckoutItems(CartProvider _cartProvider) {
    List<Result> _cartItems = _cartProvider.cartItems;
    List<Widget> _listItems = [];
    for (var i = 0; i < _cartItems.length; i++) {
      _listItems.add(CartlistItem(i));
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: []..addAll(_listItems),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _cartProvider = context.watch<CartProvider>();
    final _homeProvider = context.read<HomePageProvider>();
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        TextWidget.titleText(
          LocaleKeys.welcome_cart_msg,
          textColor: VNailColors.colorStyle5,
        ),
        SizedBox(
          height: 40,
        ),
        CurrentCartPositionWidget([true, false, false]),
        SizedBox(
          height: 40,
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextWidget.roboto400Normal(LocaleKeys.product,
                fontSize: 24, fontWeight: FontWeight.w700),
            SizedBox(
              width: DeviceUtils.isMobileDevice(context) >= 2100
                  ? 270
                  : DeviceUtils.isMobileDevice(context) >= 1500 &&
                          DeviceUtils.isMobileDevice(context) < 1700
                      ? 330
                      : DeviceUtils.isMobileDevice(context) >= 1300 &&
                              DeviceUtils.isMobileDevice(context) < 1500
                          ? 300
                          : DeviceUtils.isMobileDevice(context) >= 700 &&
                                  DeviceUtils.isMobileDevice(context) < 1000
                              ? 330
                              : DeviceUtils.isMobileDevice(context) >= 500 &&
                                      DeviceUtils.isMobileDevice(context) < 700
                                  ? 370
                                  : DeviceUtils.isMobileDevice(context) >=
                                              1000 &&
                                          DeviceUtils.isMobileDevice(context) <
                                              1200
                                      ? 350
                                      : 260,
            ),
            TextWidget.roboto400Normal(LocaleKeys.price,
                fontSize: 24, fontWeight: FontWeight.w700),
            SizedBox(
              width: DeviceUtils.isMobileDevice(context) >= 2100
                  ? 70
                  : DeviceUtils.isMobileDevice(context) >= 1500 &&
                          DeviceUtils.isMobileDevice(context) < 1700
                      ? 80
                      : DeviceUtils.isMobileDevice(context) >= 1300 &&
                              DeviceUtils.isMobileDevice(context) < 1500
                          ? 90
                          : DeviceUtils.isMobileDevice(context) >= 700 &&
                                  DeviceUtils.isMobileDevice(context) < 900
                              ? 80
                              : DeviceUtils.isMobileDevice(context) >= 500 &&
                                      DeviceUtils.isMobileDevice(context) < 700
                                  ? 70
                                  : 80,
            ),
            TextWidget.roboto400Normal(LocaleKeys.quantity,
                fontSize: 24, fontWeight: FontWeight.w700),
            SizedBox(
              width: DeviceUtils.isMobileDevice(context) >= 2100
                  ? 70
                  : DeviceUtils.isMobileDevice(context) >= 1500 &&
                          DeviceUtils.isMobileDevice(context) < 1700
                      ? 80
                      : DeviceUtils.isMobileDevice(context) >= 1300 &&
                              DeviceUtils.isMobileDevice(context) < 1500
                          ? 80
                          : 70,
            ),
            TextWidget.roboto400Normal(LocaleKeys.subtotal,
                fontSize: 24, fontWeight: FontWeight.w700),
            TextWidget.roboto400Normal(LocaleKeys.empty,
                fontSize: 24, fontWeight: FontWeight.w700),
          ],
        ),
        Divider(
          height: 16,
          thickness: 2,
          color: VNailColors.colorStyle5,
        ),
        SizedBox(
          height: 40,
        ),
        _buildCheckoutItems(_cartProvider),
        Divider(
          height: 16,
          thickness: 2,
          color: VNailColors.colorStyle5,
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 10,
              child: TextWidget.roboto400Normal(LocaleKeys.total,
                  fontSize: 24, fontWeight: FontWeight.w700),
            ),
            Expanded(
              flex: 2,
              child: TextWidget.productDescription(
                  '${_cartProvider.totalItem} items',
                  fontSize: 24,
                  fontWeight: FontWeight.w700),
            ),
            Expanded(
              flex: 2,
              child: Center(
                child: TextWidget.productDescription(
                    '${_cartProvider.totalPrice} \$',
                    fontSize: 24,
                    fontWeight: FontWeight.w700),
              ),
            ),
            Expanded(
              flex: 1,
              child: TextWidget.roboto400Normal(LocaleKeys.empty,
                  fontSize: 24, fontWeight: FontWeight.w700),
            ),
            CustomButton(
              tr(LocaleKeys.next),
              Icons.chevron_right_rounded,
              () => (_cartProvider.cartItems.isEmpty)
                  ? CoolAlert.show(
                      context: context,
                      type: CoolAlertType.warning,
                      text: tr(LocaleKeys.cart_empty_msg),
                    )
                  : _homeProvider.updateHomeContent(
                      WelcomeCart(
                        PaymentSecondPage(),
                      ),
                    ),
            ),
          ],
        ),
        SizedBox(
          height: DeviceUtils.isMobileDevice(context) >= 2900
              ? 1200
              : DeviceUtils.isMobileDevice(context) >= 2300 &&
                      DeviceUtils.isMobileDevice(context) < 2900
                  ? 1000
                  : DeviceUtils.isMobileDevice(context) >= 2000 &&
                          DeviceUtils.isMobileDevice(context) < 2300
                      ? 900
                      : DeviceUtils.isMobileDevice(context) >= 1700 &&
                              DeviceUtils.isMobileDevice(context) < 2000
                          ? 470
                          : DeviceUtils.isMobileDevice(context) >= 1400 &&
                                  DeviceUtils.isMobileDevice(context) < 1700
                              ? 300
                              : 0,
        )
      ],
    );
  }
}

class CurrentCartPositionWidget extends StatelessWidget {
  final List<bool> currentChoice;

  CurrentCartPositionWidget(
    this.currentChoice, {
    Key? key,
  }) : super(key: key);

  buildAppropriateWidget(bool isSelected, String text) {
    return SelectedTextWidget(
      text,
      isSelected,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 40, top: 24, bottom: 24, right: 40),
      decoration: BoxDecoration(
        color: VNailColors.colorStyle3,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          buildAppropriateWidget(
            currentChoice[0],
            LocaleKeys.review_order,
          ),
          buildAppropriateWidget(
            currentChoice[1],
            LocaleKeys.address,
          ),
          buildAppropriateWidget(
            currentChoice[2],
            LocaleKeys.confirm_order,
          ),
        ],
      ),
    );
  }
}

class SelectedTextWidget extends StatelessWidget {
  final String text;
  final bool isVisible;

  SelectedTextWidget(this.text, this.isVisible);

  @override
  Widget build(BuildContext context) {
    final _shadowColor =
        (isVisible) ? VNailColors.colorStyle5 : Colors.transparent;

    final _textColor =
        (isVisible) ? Colors.transparent : VNailColors.colorStyle4;

    final _textDecoration =
        (isVisible) ? TextDecoration.underline : TextDecoration.none;

    final _shadow = (isVisible)
        ? Shadow(
            offset: Offset(0, -2),
            color: _shadowColor,
          )
        : Shadow();

    return AutoSizeText(
      tr(text),
      softWrap: true,
      style: GoogleFonts.roboto(
        fontSize: 30,
        fontWeight: FontWeight.w700,
        color: _textColor,
        decoration: _textDecoration,
        decorationColor: VNailColors.colorStyle5,
        decorationThickness: 2,
        shadows: [_shadow],
      ),
    );
  }
}

class CartlistItem extends StatelessWidget {
  final int index;

  CartlistItem(
    this.index,
  );

  @override
  Widget build(BuildContext context) {
    final _cartProvider = context.watch<CartProvider>();
    final _individualItem = _cartProvider.cartItems[index];
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              width: MediaQueryUtils(context).width * 0.35,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Image(
                        image: Assets.images.bodyCareProduct,
                        fit: BoxFit.contain,
                        height: 130,
                        width: 124,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 24,
                  ),
                  Expanded(
                    flex: 3,
                    child: SizedBox(
                      width: MediaQueryUtils(context).width * 0.25,
                      child: TextWidget.productDescription(
                        _individualItem.name,
                        fontSize: 24,
                        textColor: VNailColors.colorStyle4,
                        maxLines: 3,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 20,
            ),
            TextWidget.productDescription(
              '${_individualItem.price.toString()} \$',
              fontSize: 24,
              textColor: VNailColors.colorStyle4,
            ),
            SizedBox(
              width: DeviceUtils.isMobileDevice(context) >= 2000 ? 0 : 10,
            ),
            VolumeCounterButton(
              _individualItem.quantity,
              () => _cartProvider.increaseVolume(index),
              () => _cartProvider.decreaseVolume(index),
            ),
            SizedBox(
              width: DeviceUtils.isMobileDevice(context) >= 2000 ? 0 : 10,
            ),
            TextWidget.productDescription(
              '${_individualItem.price * _individualItem.quantity} \$',
              fontSize: 24,
              textColor: VNailColors.colorStyle4,
            ),
            SizedBox(
              width: 30,
            ),
            CustomButton(
              tr(LocaleKeys.remove),
              Icons.delete_outline,
              () => _cartProvider.removeProductFromCart(_individualItem),
              width: MediaQueryUtils(context).width * 0.15,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}

class VolumeCounterButton extends StatelessWidget {
  final int quantity;
  final VoidCallback onIncreaseClick, onDecreaseClick;

  VolumeCounterButton(
      this.quantity, this.onIncreaseClick, this.onDecreaseClick);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularButton(
            Icons.remove,
            onDecreaseClick,
          ),
          ItemCount(quantity),
          CircularButton(
            Icons.add,
            onIncreaseClick,
          ),
        ],
      ),
    );
  }
}

class PaymentSecondPage extends StatefulWidget {
  const PaymentSecondPage({
    Key? key,
  }) : super(key: key);

  @override
  _PaymentSecondPageState createState() => _PaymentSecondPageState();
}

class _PaymentSecondPageState extends State<PaymentSecondPage> {
  GlobalKey<FormState> _billingFormKey = GlobalKey<FormState>();
  GlobalKey<FormState> _shippingFormKey = GlobalKey<FormState>();

  TextEditingController _nameController = TextEditingController(),
      _mobileController = TextEditingController(),
      _emailController = TextEditingController(),
      _numberStreetController = TextEditingController(),
      _streetController = TextEditingController(),
      _cityController = TextEditingController();

  FocusNode _nameNode = FocusNode(),
      _mobileNode = FocusNode(),
      _emailNode = FocusNode(),
      _numberStreetNode = FocusNode(),
      _streetNode = FocusNode(),
      _cityNode = FocusNode();

  TextEditingController _nameShippingController = TextEditingController(),
      _mobileShippingController = TextEditingController(),
      _emailShippingController = TextEditingController(),
      _numberStreetShippingController = TextEditingController(),
      _streetShippingController = TextEditingController(),
      _cityShippingController = TextEditingController();

  FocusNode _nameShippingNode = FocusNode(),
      _mobileShippingNode = FocusNode(),
      _emailShippingNode = FocusNode(),
      _numberStreetShippingNode = FocusNode(),
      _streetShippingNode = FocusNode(),
      _cityShippingNode = FocusNode();

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _mobileController.dispose();
    _emailController.dispose();
    _numberStreetController.dispose();
    _streetController.dispose();
    _cityController.dispose();
    _nameNode.dispose();
    _mobileNode.dispose();
    _emailNode.dispose();
    _numberStreetNode.dispose();
    _streetNode.dispose();
    _cityNode.dispose();
    _nameShippingController.dispose();
    _mobileShippingController.dispose();
    _emailShippingController.dispose();
    _numberStreetShippingController.dispose();
    _streetShippingController.dispose();
    _cityShippingController.dispose();
    _nameShippingNode.dispose();
    _mobileShippingNode.dispose();
    _emailShippingNode.dispose();
    _numberStreetShippingNode.dispose();
    _streetShippingNode.dispose();
    _cityShippingNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _homeProvider = context.read<HomePageProvider>();
    final _cartProvider = context.watch<CartProvider>();
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: TextWidget.titleText(
            LocaleKeys.address,
            textColor: VNailColors.colorStyle5,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        CurrentCartPositionWidget([false, true, false]),
        SizedBox(
          height: 40,
        ),
        Form(
          key: _billingFormKey,
          child: BillingAddressFormWidget(
            _nameController,
            _nameNode,
            _mobileController,
            _mobileNode,
            _emailController,
            _emailNode,
            _numberStreetController,
            _numberStreetNode,
            _streetController,
            _streetNode,
            _cityController,
            _cityNode,
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Checkbox(
              value: _cartProvider.sameShippingAddress,
              onChanged: (value) {
                if (value == true) {
                  _nameShippingController.text = _nameController.text;
                  _mobileShippingController.text = _mobileController.text;
                  _emailShippingController.text = _emailController.text;
                  _numberStreetShippingController.text =
                      _numberStreetController.text;
                  _streetShippingController.text = _streetController.text;
                  _cityShippingController.text = _cityController.text;
                } else {
                  _nameShippingController.text = "";
                  _mobileShippingController.text = "";
                  _emailShippingController.text = "";
                  _numberStreetShippingController.text = "";
                  _streetShippingController.text = "";
                  _cityShippingController.text = "";
                }
                _cartProvider.toggleShippingAddress(value);
              },
            ),
            SizedBox(
              width: 8,
            ),
            TextWidget.roboto400Normal(LocaleKeys.same_address_msg),
            Expanded(child: SizedBox()),
          ],
        ),
        Form(
          key: _shippingFormKey,
          child: ShippingAddressFormWidget(
            _nameShippingController,
            _nameShippingNode,
            _mobileShippingController,
            _mobileShippingNode,
            _emailShippingController,
            _emailShippingNode,
            _numberStreetShippingController,
            _numberStreetShippingNode,
            _streetShippingController,
            _streetShippingNode,
            _cityShippingController,
            _cityShippingNode,
          ),
        ),
        SizedBox(
          height: 40,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 128, right: 128),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomBackButton(
                LocaleKeys.back,
                Icons.chevron_left_rounded,
                () => _homeProvider.updateHomeContent(
                  WelcomeCart(
                    PaymentFirstPage(),
                  ),
                ),
              ),
              CustomButton(LocaleKeys.next, Icons.chevron_right_rounded, () {
                if (_billingFormKey.currentState!.validate() &&
                    _shippingFormKey.currentState!.validate()) {
                  _cartProvider.setBillingData(
                      _nameController.text,
                      _mobileController.text,
                      _emailController.text,
                      _numberStreetController.text,
                      _streetController.text,
                      _cityController.text,
                      _nameShippingController.text,
                      _mobileShippingController.text,
                      _emailShippingController.text,
                      _numberStreetShippingController.text,
                      _streetShippingController.text,
                      _cityShippingController.text);

                  _cartProvider.shippingAddressFalse();

                  _homeProvider.updateHomeContent(
                    WelcomeCart(
                      PaymentThirdPage(),
                    ),
                  );
                }
              }),
            ],
          ),
        ),
        SizedBox(
          height: 128,
        ),
      ],
    );
  }
}

class BillingAddressFormWidget extends StatelessWidget {
  final TextEditingController nameController,
      mobileController,
      emailController,
      numberStreetController,
      streetController,
      cityController;

  final FocusNode nameNode,
      mobileNode,
      emailNode,
      numberStreetNode,
      streetNode,
      cityNode;

  BillingAddressFormWidget(
    this.nameController,
    this.nameNode,
    this.mobileController,
    this.mobileNode,
    this.emailController,
    this.emailNode,
    this.numberStreetController,
    this.numberStreetNode,
    this.streetController,
    this.streetNode,
    this.cityController,
    this.cityNode,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: TextWidget.titleText(
              LocaleKeys.billing_address,
              fontSize: 36,
              fontWeight: FontWeight.w700,
              textColor: VNailColors.colorStyle5,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextFieldWidget(LocaleKeys.name_hint, nameNode, nameController),
          TextFieldWidget(
              LocaleKeys.mobile_number_hint, mobileNode, mobileController),
          TextFieldWidget(LocaleKeys.email, emailNode, emailController),
          TextFieldWidget(LocaleKeys.number_street, numberStreetNode,
              numberStreetController),
          TextFieldWidget(LocaleKeys.street2, streetNode, streetController),
          TextFieldWidget(LocaleKeys.city, cityNode, cityController),
        ],
      ),
    );
  }
}

class ShippingAddressFormWidget extends StatelessWidget {
  final TextEditingController nameController,
      mobileController,
      emailController,
      numberStreetController,
      streetController,
      cityController;

  final FocusNode nameNode,
      mobileNode,
      emailNode,
      numberStreetNode,
      streetNode,
      cityNode;

  ShippingAddressFormWidget(
    this.nameController,
    this.nameNode,
    this.mobileController,
    this.mobileNode,
    this.emailController,
    this.emailNode,
    this.numberStreetController,
    this.numberStreetNode,
    this.streetController,
    this.streetNode,
    this.cityController,
    this.cityNode,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: TextWidget.titleText(
              LocaleKeys.shipping_address,
              fontSize: 36,
              fontWeight: FontWeight.w700,
              textColor: VNailColors.colorStyle5,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextFieldWidget(LocaleKeys.name_hint, nameNode, nameController),
          TextFieldWidget(
              LocaleKeys.mobile_number_hint, mobileNode, mobileController),
          TextFieldWidget(LocaleKeys.email, emailNode, emailController),
          TextFieldWidget(LocaleKeys.number_street, numberStreetNode,
              numberStreetController),
          TextFieldWidget(LocaleKeys.street2, streetNode, streetController),
          TextFieldWidget(LocaleKeys.city, cityNode, cityController),
        ],
      ),
    );
  }
}

class PaymentThirdPage extends StatelessWidget {
  const PaymentThirdPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _homeProvider = context.read<HomePageProvider>();
    final _cartProvider = context.read<CartProvider>();

    String _billingAddress = _cartProvider.billingStreetNumber +
        " " +
        _cartProvider.billingStreet +
        " " +
        _cartProvider.billingCity;

    String _shippingAddress = _cartProvider.shippingStreetNumber +
        " " +
        _cartProvider.shippingStreet +
        " " +
        _cartProvider.shippingCity;

    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextWidget.titleText(
          LocaleKeys.payment,
          textColor: VNailColors.colorStyle5,
        ),
        SizedBox(
          height: 40,
        ),
        CurrentCartPositionWidget([false, false, true]),
        SizedBox(
          height: 40,
        ),
        Stack(
          children: [
            CustomCardWidget(
              SizedBox(
                height: 128,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: AddressWidget(
                        LocaleKeys.billing_address,
                        _billingAddress,
                      ),
                      flex: 3,
                    ),
                    Expanded(
                      flex: 2,
                      child: VerticalDivider(
                        color: VNailColors.colorStyle5,
                        thickness: 2,
                      ),
                    ),
                    Expanded(
                      child: AddressWidget(
                        LocaleKeys.shipping_address,
                        _shippingAddress,
                      ),
                      flex: 3,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              child: IconButton(
                icon: Icon(
                  Icons.edit_rounded,
                  color: VNailColors.colorStyle5,
                ),
                onPressed: () => _homeProvider.updateHomeContent(
                  WelcomeCart(
                    PaymentSecondPage(),
                  ),
                ),
              ),
              right: 16,
              top: 16,
            ),
          ],
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              flex: 2,
              child: CustomCardWidget(
                ProductListWidget(),
              ),
            ),
            SizedBox(
              width: 80,
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16),
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(4, 4),
                      color: Colors.black12,
                      spreadRadius: 2,
                      blurRadius: 2,
                    )
                  ],
                ),
                child: PaymentCard(),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 40,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            CustomBackButton(
              LocaleKeys.return_cart,
              Icons.chevron_left_rounded,
              () => _homeProvider.updateHomeContent(
                WelcomeCart(
                  PaymentFirstPage(),
                ),
              ),
            ),
            CustomButton(
              LocaleKeys.pay_now,
              Icons.chevron_right_rounded,
              () async {
                final _url = await _cartProvider.openCheckoutPage(context);
                if (await canLaunch(_url)) {
                  launch(_url);
                }
              },
              width: MediaQueryUtils(context).width * 0.15,
            ),
          ],
        ),
        SizedBox(
          height: 80,
        ),
      ],
    );
  }
}

class AddressWidget extends StatelessWidget {
  final String text1, text2;

  AddressWidget(this.text1, this.text2);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextWidget.roboto400Normal(text1,
            fontSize: 24, fontWeight: FontWeight.w700),
        SizedBox(
          height: 16,
        ),
        TextWidget.productDescription(
          text2,
          maxLines: 3,
          fontSize: 18,
        ),
      ],
    );
  }
}

class CustomCardWidget extends StatelessWidget {
  final Widget child;

  CustomCardWidget(this.child);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(80, 40, 80, 40),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            offset: Offset(4, 4),
            color: Colors.black12,
            spreadRadius: 2,
            blurRadius: 2,
          )
        ],
      ),
      child: child,
    );
  }
}

class ProductListWidget extends StatelessWidget {
  Widget _buildListOfProducts(List<Result> _results) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        Result _individualResult = _results[index];
        return ProductItem(
          _individualResult.name,
          _individualResult.quantity,
          _individualResult.subTotal,
        );
      },
      itemCount: _results.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    final _cartProvider = context.watch<CartProvider>();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RowItemWidget(
          TextWidget.roboto400Normal(LocaleKeys.product,
              fontWeight: FontWeight.w500),
          TextWidget.roboto400Normal(LocaleKeys.quantity,
              fontWeight: FontWeight.w500),
          TextWidget.roboto400Normal(LocaleKeys.price,
              fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: 24,
        ),
        _buildListOfProducts(_cartProvider.cartItems),
        SizedBox(
          height: 24,
        ),
        RowItemWidget(
          TextWidget.roboto400Normal(
            LocaleKeys.subtotal,
            fontWeight: FontWeight.w700,
          ),
          TextWidget.productDescription(
            _cartProvider.totalItem.toString(),
            fontWeight: FontWeight.w500,
          ),
          TextWidget.roboto400Normal(
            '${_cartProvider.totalPrice} \$',
            fontWeight: FontWeight.w500,
          ),
        ),
        RowItemWidget(
          TextWidget.roboto400Normal(
            LocaleKeys.shipping_cost,
            fontWeight: FontWeight.w700,
          ),
          TextWidget.roboto400Normal(
            LocaleKeys.empty,
            fontWeight: FontWeight.w500,
          ),
          TextWidget.roboto400Normal(
            '${_cartProvider.getShippingCharge()} \$',
            fontWeight: FontWeight.w500,
          ),
        ),
        RowItemWidget(
          TextWidget.roboto400Normal(
            LocaleKeys.loyaly_balance,
            fontWeight: FontWeight.w700,
          ),
          TextWidget.roboto400Normal(
            LocaleKeys.empty,
            fontWeight: FontWeight.w500,
          ),
          TextWidget.roboto400Normal(
            '${_cartProvider.loyaltyPointConversion.toString()} \$',
            fontWeight: FontWeight.w500,
          ),
        ),
        Divider(
          height: 32,
          color: VNailColors.colorStyle5,
        ),
        RowItemWidget(
          TextWidget.roboto400Normal(
            LocaleKeys.total,
            fontWeight: FontWeight.w700,
          ),
          TextWidget.roboto400Normal(
            LocaleKeys.empty,
            fontWeight: FontWeight.w500,
          ),
          TextWidget.roboto400Normal(
            (_cartProvider.totalPrice + _cartProvider.getShippingCharge())
                .toString(),
            fontWeight: FontWeight.w500,
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
              onPressed: () => _cartProvider.toggleUsingLoyaltyPoints(),
              icon: Icon(
                (_cartProvider.isUsingLoyaltyPoints)
                    ? Icons.check_circle_outline_sharp
                    : Icons.add_circle_outline,
              ),
              color: VNailColors.colorStyle5,
            ),
            SizedBox(
              width: 8,
            ),
            TextWidget.roboto400Normal(LocaleKeys.use_loyalty_points),
            SizedBox(
              width: 200,
            )
          ],
        ),
      ],
    );
  }
}

class ProductItem extends StatelessWidget {
  final String productName;
  final int productQuantity, productSubTotal;

  ProductItem(
    this.productName,
    this.productQuantity,
    this.productSubTotal,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 6,
              child: Row(
                children: [
                  Flexible(
                    flex: 1,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Image(
                        image: Assets.images.bodyCareProduct,
                        fit: BoxFit.contain,
                        height: 92,
                        width: 88,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Flexible(
                    flex: 2,
                    child: TextWidget.productDescription(productName),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TextWidget.productDescription(productQuantity.toString()),
              flex: 2,
            ),
            Expanded(
              child: TextWidget.productDescription(
                  '${productSubTotal.toString()} \$'),
              flex: 2,
            ),
          ],
        ),
        SizedBox(
          height: 24,
        ),
      ],
    );
  }
}

class RowItemWidget extends StatelessWidget {
  final Widget widget1, widget2, widget3;

  RowItemWidget(this.widget1, this.widget2, this.widget3);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: widget1,
              flex: 6,
            ),
            Expanded(
              child: widget2,
              flex: 2,
            ),
            Expanded(
              child: widget3,
              flex: 2,
            ),
          ],
        ),
        SizedBox(
          height: 24,
        )
      ],
    );
  }
}

class PaymentCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextWidget.roboto400Normal(
            LocaleKeys.pay_with,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Radio(
              value: true,
              groupValue: true,
              onChanged: (bool) => null,
              fillColor: MaterialStateProperty.resolveWith(
                  (states) => VNailColors.colorStyle5),
            ),
            SizedBox(
              width: 8,
            ),
            Expanded(
              flex: 1,
              child: TextWidget.roboto400Normal(
                LocaleKeys.credit_card,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 48,
        )
      ],
    );
  }
}
