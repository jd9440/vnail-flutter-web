import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/providers/product_provider.dart';
import 'package:vnail/response/product_response.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/text.dart';

import 'pageimage.dart';
import 'product_description.dart';

class ProductContent extends StatelessWidget {
  final int selectedIndex;

  ProductContent(this.selectedIndex);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          image: Assets.images.productsBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        columnMainAxisSize: MainAxisSize.max,
        columnMainAxisAlignment: MainAxisAlignment.start,
        columnCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            child: PageImage(
              LocaleKeys.product_header_txt,
              Assets.images.products,
            ),
          ),
          ResponsiveRowColumnItem(
            child: ResponsiveRowColumn(
              layout: ResponsiveRowColumnType.ROW,
              rowMainAxisSize: MainAxisSize.min,
              rowCrossAxisAlignment: CrossAxisAlignment.start,
              rowMainAxisAlignment: MainAxisAlignment.start,
              children: [
                ResponsiveRowColumnItem(
                  child: Expanded(
                    flex: 3,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(42, 100, 42, 84),
                      child:
                          ProductsCategoryWidget(selectedIndex: selectedIndex),
                    ),
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: SizedBox(
                    width: 16,
                  ),
                ),
                ResponsiveRowColumnItem(
                  child: ProductGridWidget(selectedIndex: selectedIndex),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ProductGridWidget extends StatelessWidget {
  const ProductGridWidget({
    Key? key,
    required this.selectedIndex,
  }) : super(key: key);

  final int selectedIndex;

  @override
  Widget build(BuildContext context) {
    final _productProvider = Provider.of<ProductProvider>(context);
    return ResponsiveRowColumnItem(
      child: Expanded(
        flex: 7,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 80, 84, 84),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextWidget.titleText(
                _productProvider.currentProductTitle(selectedIndex),
                fontSize: 50,
                textColor: VNailColors.colorStyle5,
              ),
              Text.rich(
                TextSpan(
                  text: tr(LocaleKeys.product_home_path),
                  style: GoogleFonts.roboto(
                    fontSize: 24,
                    fontWeight: FontWeight.w400,
                    color: VNailColors.colorStyle4,
                  ),
                  children: <InlineSpan>[
                    TextSpan(
                      text: tr(
                          _productProvider.currentProductTitle(selectedIndex)),
                      style: GoogleFonts.roboto(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        color: VNailColors.colorStyle5,
                      ),
                    )
                  ],
                ),
              ),
              ProductsGrid(currentSelectedCategory: selectedIndex),
            ],
          ),
        ),
      ),
    );
  }
}

class ProductsCategoryWidget extends StatelessWidget {
  const ProductsCategoryWidget({
    Key? key,
    required this.selectedIndex,
  }) : super(key: key);

  final int selectedIndex;

  @override
  Widget build(BuildContext context) {
    final _homeProvider = Provider.of<HomePageProvider>(context);
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnCrossAxisAlignment: CrossAxisAlignment.start,
      columnMainAxisAlignment: MainAxisAlignment.start,
      columnMainAxisSize: MainAxisSize.min,
      children: [
        ResponsiveRowColumnItem(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: TextWidget.roboto400Normal(
              LocaleKeys.product_category,
              fontSize: 30,
              fontWeight: FontWeight.w700,
              textColor: VNailColors.colorStyle5,
            ),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(
            height: 20,
          ),
        ),
        ResponsiveRowColumnItem(
          child: TextWidget.footerTextStyle(
            LocaleKeys.all_products,
            fontSize: 24,
            fontWeight:
                (selectedIndex == 0) ? FontWeight.w700 : FontWeight.w400,
            onClickAction: () =>
                _homeProvider.updateHomeContent(ProductContent(0)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(
            height: 20,
          ),
        ),
        ResponsiveRowColumnItem(
          child: TextWidget.footerTextStyle(
            LocaleKeys.body_care,
            fontSize: 24,
            fontWeight:
                (selectedIndex == 1) ? FontWeight.w700 : FontWeight.w400,
            onClickAction: () =>
                _homeProvider.updateHomeContent(ProductContent(1)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(
            height: 20,
          ),
        ),
        ResponsiveRowColumnItem(
          child: TextWidget.footerTextStyle(
            LocaleKeys.foot,
            fontSize: 24,
            fontWeight:
                (selectedIndex == 2) ? FontWeight.w700 : FontWeight.w400,
            onClickAction: () =>
                _homeProvider.updateHomeContent(ProductContent(2)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(
            height: 20,
          ),
        ),
        ResponsiveRowColumnItem(
          child: TextWidget.footerTextStyle(
            LocaleKeys.hand_nail,
            fontSize: 24,
            fontWeight:
                (selectedIndex == 3) ? FontWeight.w700 : FontWeight.w400,
            onClickAction: () =>
                _homeProvider.updateHomeContent(ProductContent(3)),
          ),
        ),
        ResponsiveRowColumnItem(
          child: SizedBox(
            height: 20,
          ),
        ),
        ResponsiveRowColumnItem(
          child: TextWidget.footerTextStyle(
            LocaleKeys.others,
            fontSize: 24,
            fontWeight:
                (selectedIndex == 4) ? FontWeight.w700 : FontWeight.w400,
            onClickAction: () =>
                _homeProvider.updateHomeContent(ProductContent(4)),
          ),
        ),
      ],
    );
  }
}

class ProductsGrid extends StatelessWidget {
  const ProductsGrid({
    Key? key,
    this.currentSelectedCategory = 0,
  }) : super(key: key);

  final int currentSelectedCategory;

  List<Result> _returnUpdatedProductsList(ProductProvider _productProvider) {
    switch (currentSelectedCategory) {
      case 0:
        return _productProvider.fetchAllProducts;
      case 1:
        return _productProvider.fetchBodyCareProducts;
      case 2:
        return _productProvider.fetchFootProducts;
      case 3:
        return _productProvider.fetchHandNailProducts;
      default:
        return _productProvider.fetchOtherProducts;
    }
  }

  @override
  Widget build(BuildContext context) {
    final _productProvider = Provider.of<ProductProvider>(context);

    final _homeProvider = Provider.of<HomePageProvider>(context);
    List<Result> _resultantItems = _returnUpdatedProductsList(_productProvider);
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.only(top: 40),
      shrinkWrap: true,
      itemCount: _resultantItems.length,
      itemBuilder: (context, index) {
        Result _individualProduct = _resultantItems[index];
        return InkWell(
          onTap: () {
            _productProvider.addRecentlyViewedItem(_individualProduct);
            _homeProvider.updateHomeContent(
              ProductDescriptionContent(
                _individualProduct,
              ),
            );
          },
          child: ProductItemWidget(
            _individualProduct.name,
            _individualProduct.price.toString(),
          ),
        );
      },
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        childAspectRatio: 0.7,
      ),
    );
  }
}

class ProductItemWidget extends StatelessWidget {
  final String productString, price;

  ProductItemWidget(this.productString, this.price);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 8),
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 8,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Image(
                image: Assets.images.bodyCareProduct,
                fit: BoxFit.contain,
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: TextWidget.productDescription(
              productString,
              maxLines: 2,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: TextWidget.productDescription(
              '$price \$',
              maxLines: 2,
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
