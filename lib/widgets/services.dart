import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:vnail/gen/assets.gen.dart';
import 'package:vnail/generated/locale_keys.g.dart';
import 'package:vnail/providers/home_provider.dart';
import 'package:vnail/utility/color.dart';
import 'package:vnail/utility/mediaquery.dart';
import 'package:vnail/utility/text.dart';
import 'package:vnail/utility/url_services.dart';

class ServiceContent extends StatelessWidget {
  final int index;

  ServiceContent(this.index);

  @override
  Widget build(BuildContext context) {
    return ResponsiveRowColumn(
      layout: ResponsiveRowColumnType.COLUMN,
      columnMainAxisAlignment: MainAxisAlignment.start,
      columnMainAxisSize: MainAxisSize.min,
      children: [
        ResponsiveRowColumnItem(child: Service(index)),
        ResponsiveRowColumnItem(child: ServiceWidget(index)),
      ],
    );
  }
}

class Service extends StatelessWidget {
  final int index;
  Service(this.index);
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        ColorFiltered(
          colorFilter: ColorFilter.mode(
            Colors.black12,
            BlendMode.color,
          ),
          child: Image(
            image: Assets.images.serviceBg,
            fit: BoxFit.fill,
            width: MediaQueryUtils(context).width,
            height: MediaQueryUtils(context).height *
              0.4 *
                MediaQueryUtils(context).aspectRatio,
          ),
        ),
        TextWidget.titleText(
          (index == 0)
              ? LocaleKeys.manicure
              : (index == 1)
                  ? LocaleKeys.pedicure
                  : LocaleKeys.treatment,
          isUpperCase: true,
        ),
      ],
    );
  }
}

class ServiceWidget extends StatelessWidget {
  final int index;

  ServiceWidget(this.index);

  @override
  Widget build(BuildContext context) {
    return ServicesWebTabletWidget(index: index);
  }
}

// class ServicesMobileWidget extends StatelessWidget {
//   const ServicesMobileWidget({
//     Key? key,
//     required this.index,
//   }) : super(key: key);

//   final int index;

//   @override
//   Widget build(BuildContext context) {
//     final _provider = context.watch<HomePageProvider>();
//     return Container(
//       height: MediaQueryUtils(context).height,
//       padding: EdgeInsets.only(left: 48, right: 48, top: 32, bottom: 54),
//       decoration: BoxDecoration(
//         image: DecorationImage(
//           image: Assets.images.designerBg,
//           alignment: Alignment.center,
//           fit: BoxFit.cover,
//         ),
//       ),
//       child: ResponsiveRowColumn(
//         layout: ResponsiveRowColumnType.COLUMN,
//         children: [
//           ResponsiveRowColumnItem(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.start,
//               mainAxisSize: MainAxisSize.max,
//               children: [
//                 TextWidget.footerTextStyle(
//                   LocaleKeys.manicure,
//                   fontSize: 24,
//                   fontWeight: FontWeight.w700,
//                   onClickAction: () =>
//                       _provider.updateHomeContent(ServiceContent(0)),
//                 ),
//                 SizedBox(
//                   width: 24,
//                 ),
//                 TextWidget.footerTextStyle(
//                   LocaleKeys.pedicure,
//                   fontSize: 24,
//                   fontWeight: FontWeight.w700,
//                   onClickAction: () =>
//                       _provider.updateHomeContent(ServiceContent(1)),
//                 ),
//                 SizedBox(
//                   width: 24,
//                 ),
//                 TextWidget.footerTextStyle(
//                   LocaleKeys.treatment,
//                   fontSize: 24,
//                   fontWeight: FontWeight.w700,
//                   onClickAction: () =>
//                       _provider.updateHomeContent(ServiceContent(2)),
//                 ),
//               ],
//             ),
//           ),
//           ResponsiveRowColumnItem(
//             child: Padding(
//               padding: const EdgeInsets.only(
//                   left: 100, bottom: 80, right: 100, top: 80),
//               child: Image(
//                 key: UniqueKey(),
//                 image: (index == 0)
//                     ? Assets.images.manicureData
//                     : (index == 1)
//                         ? Assets.images.pedicureData
//                         : Assets.images.treatmentData,
//                 fit: BoxFit.contain,
//               ),
//             ),
//           ),
//           ResponsiveRowColumnItem(
//             child: Column(
//               mainAxisSize: MainAxisSize.min,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.only(bottom: 64),
//                   child: TextWidget.serviceTitleText(
//                     LocaleKeys.book_appointment_msg,
//                   ),
//                 ),
//                 ElevatedButton.icon(
//                   icon: Image.asset(
//                     Assets.images.whatsappIcon.path,
//                     color: Colors.white,
//                   ),
//                   label: TextWidget.serviceButtonText(
//                     LocaleKeys.booknow,
//                   ),
//                   onPressed: () => URLServiceUtils(context).openWhatsApp(),
//                   style: ElevatedButton.styleFrom(
//                     primary: VNailColors.bookNowButtonColor,
//                     shape: StadiumBorder(),
//                     minimumSize: Size(200, 48),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

class ServicesWebTabletWidget extends StatelessWidget {
  const ServicesWebTabletWidget({
    Key? key,
    required this.index,
  }) : super(key: key);

  final int index;

  @override
  Widget build(BuildContext context) {
    final _provider = context.watch<HomePageProvider>();
    return Container(
      padding: EdgeInsets.only(left: 48, right: 48, top: 32, bottom: 54),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Assets.images.designerBg,
          alignment: Alignment.center,
          fit: BoxFit.cover,
        ),
      ),
      child: ResponsiveRowColumn(
        layout: ResponsiveRowColumnType.COLUMN,
        children: [
          ResponsiveRowColumnItem(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                TextWidget.footerTextStyle(
                  LocaleKeys.manicure,
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  onClickAction: () =>
                      _provider.updateHomeContent(ServiceContent(0)),
                ),
                SizedBox(
                  width: 24,
                ),
                TextWidget.footerTextStyle(
                  LocaleKeys.pedicure,
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  onClickAction: () =>
                      _provider.updateHomeContent(ServiceContent(1)),
                ),
                SizedBox(
                  width: 24,
                ),
                TextWidget.footerTextStyle(
                  LocaleKeys.treatment,
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  onClickAction: () =>
                      _provider.updateHomeContent(ServiceContent(2)),
                ),
              ],
            ),
          ),
          ResponsiveRowColumnItem(
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 100, bottom: 80, right: 100, top: 80),
              child: Image(
                key: UniqueKey(),
                image: (index == 0)
                    ? Assets.images.manicureData
                    : (index == 1)
                        ? Assets.images.pedicureData
                        : Assets.images.treatmentData,
                fit: BoxFit.contain,
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 64),
                  child: TextWidget.serviceTitleText(
                    LocaleKeys.book_appointment_msg,
                  ),
                ),
                ElevatedButton.icon(
                  icon: Image.asset(
                    Assets.images.whatsappIcon.path,
                    color: Colors.white,
                  ),
                  label: TextWidget.serviceButtonText(
                    LocaleKeys.booknow,
                  ),
                  onPressed: () => URLServiceUtils(context).openWhatsApp(),
                  style: ElevatedButton.styleFrom(
                    primary: VNailColors.bookNowButtonColor,
                    shape: StadiumBorder(),
                    minimumSize: Size(200, 48),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ServiceItemWidget extends StatelessWidget {
  late final String imagePath, serviceName;
  late final VoidCallback? onClickAction;

  ServiceItemWidget(this.imagePath, this.serviceName, {this.onClickAction});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(34),
          child: Image.asset(
            imagePath,
            height: MediaQueryUtils(context).width * 0.15,
            width: MediaQueryUtils(context).width * 0.12,
            isAntiAlias: true,
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 16),
          child: TextWidget.serviceNameText(
            serviceName,
          ),
        ),
        //  TODO : Implement onClickAction for book now button on service part
        ElevatedButton(
          onPressed: () {},
          child: TextWidget.serviceButtonText(LocaleKeys.booknow),
          style: ElevatedButton.styleFrom(
            primary: VNailColors.colorStyle3,
            shape: StadiumBorder(),
            minimumSize: Size(140, 48),
          ),
        )
      ],
    );
  }
}

class ProductsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.only(
        top: MediaQueryUtils(context).height * 0.05,
        bottom: MediaQueryUtils(context).height * 0.1,
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: Assets.images.productBgImage,
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 80, bottom: 80),
            child: TextWidget.serviceTitleText(LocaleKeys.our_products,
                textColor: Colors.white),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ProductItemWidget(
                Assets.images.bodyCareProduct.path,
                LocaleKeys.body_care,
              ),
              ProductItemWidget(
                Assets.images.footProduct.path,
                LocaleKeys.foot,
              ),
              ProductItemWidget(
                Assets.images.handNailProduct.path,
                LocaleKeys.hand_nail,
              ),
              ProductItemWidget(
                Assets.images.otherProduct.path,
                LocaleKeys.others,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ProductItemWidget extends StatelessWidget {
  late final String imagePath, serviceName;
  late final VoidCallback? onClickAction;

  ProductItemWidget(this.imagePath, this.serviceName, {this.onClickAction});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12, right: 12),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(34),
            child: Image.asset(
              imagePath,
              isAntiAlias: true,
              fit: BoxFit.fill,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 16),
          child: TextWidget.serviceNameText(
            serviceName,
            fontColor: Colors.white,
          ),
        ),
      ],
    );
  }
}
